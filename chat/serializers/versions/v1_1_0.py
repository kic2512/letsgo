from django.utils import timezone
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from chat.models import Message, Chat, MessageReadState, MuteTable
from core.serializers import UserSerializer

VERSION = '1-1-0'


class MessageSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    is_my = SerializerMethodField(read_only=True)
    date_create = SerializerMethodField(read_only=True)

    def get_is_my(self, obj):
        return self.context['request'].user == obj.author

    def get_date_create(self, obj):
        return str(timezone.localtime(obj.date_create))

    class Meta:
        model = Message
        fields = ('text', 'author', 'is_read', 'is_received', 'date_create', 'is_my')


class ChatSerializer(serializers.ModelSerializer):

    owner = UserSerializer()
    users = UserSerializer(many=True)
    title = serializers.SerializerMethodField()
    last_message = serializers.SerializerMethodField()
    unread_count = serializers.SerializerMethodField()
    is_mute = serializers.SerializerMethodField()
    users_count = serializers.SerializerMethodField()
    interlocutor = serializers.SerializerMethodField()

    def get_title(self, obj):
        return obj.get_title(self.context['request'].user)

    def get_users_count(self, obj):
        return obj.users.count()

    def get_last_message(self, obj):
        last_msg = obj.messages.filter(chat_id=obj.id).order_by('id').last()

        return MessageSerializer(last_msg, context=self.context).data

    def get_unread_count(self, obj):
        user = self.context['request'].user
        try:
            return MessageReadState.objects.get(user=user, chat=obj).get_unread_count()
        except MessageReadState.DoesNotExist:
            return Message.objects.filter(chat=obj).count()

    def get_is_mute(self, obj):
        return MuteTable.objects.filter(
            chat=obj,
            user=self.context['request'].user,
            is_active=True
        ).exists()

    def get_interlocutor(self, obj):
        if obj.owner.pk == self.context['request'].user.pk:
            user = obj.users.filter().first()
            if user:
                return UserSerializer(user).data
        else:
            return UserSerializer(obj.owner).data

    class Meta:
        model = Chat
        fields = (
            'id',
            'title',
            'owner',
            'users',
            'channel_slug',
            'last_message',
            'unread_count',
            'is_mute',
            'users_count',
            'interlocutor'
        )
