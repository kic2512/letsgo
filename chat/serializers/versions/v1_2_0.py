from rest_framework import serializers

from chat.models import Chat, MuteTable
from chat.serializers.versions.v1_1_0 import MessageSerializer
from core.serializers import UserSerializer

VERSION = '1-2-0'


class ChatMessagesSerializer(serializers.ModelSerializer):

    messages = MessageSerializer(many=True)
    users = UserSerializer(many=True)
    is_my = serializers.SerializerMethodField()
    interlocutor = serializers.SerializerMethodField()

    def get_messages(self, obj):
        return MessageSerializer(obj.messages.all(), many=True)

    def get_users(self, obj):
        return UserSerializer(obj.users.all(), many=True)

    def get_interlocutor(self, obj):
        if obj.owner.pk == self.context['request'].user.pk:
            user = obj.users.filter().first()
            if user:
                return UserSerializer(user).data
        else:
            return UserSerializer(obj.owner).data

    def get_is_my(self, obj):
        return obj.owner == self.context['request'].user

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    class Meta:
        fields = ('messages', 'users', 'is_my', 'channel_slug', 'interlocutor')
        model = Chat
