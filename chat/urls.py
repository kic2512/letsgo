from django.conf.urls import url

from chat.views import SaveMessage, ChatsList, MessagesObjects, ChatRemove, TotalUnreadMessages, MuteChat, ChatMembers

urlpatterns = [
    url(r'^save_message/', SaveMessage.as_view(), name='save_message'),
    url(r'^chats-list/', ChatsList.as_view(), name='chats-list'),
    url(r'^message-list/(?P<pk>\d+)/', MessagesObjects.as_view(), name='message-list'),
    url(r'^mute-chat/(?P<pk>\d+)/', MuteChat.as_view(), name='mute-chat'),
    url(r'^remove-chat/(?P<pk>\d+)/', ChatRemove.as_view(), name='remove-chat'),
    url(r'^total-unread/', TotalUnreadMessages.as_view(), name='unread-count'),
    url(r'^chat-members/(?P<pk>\d+)/', ChatMembers.as_view(), name='chat-members'),
]
