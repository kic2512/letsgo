import logging
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Q, F, Prefetch
from django.utils import timezone
from rest_framework.generics import ListAPIView, DestroyAPIView, UpdateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from application.settings.base import REDIS_DB
from chat.constants import REDIS_UNREAD_USERS_KEY
from chat.mixins import ChatMixin, MessageMixin, MuteMixin, ChatUsersMixin, ChatMembersMixin
from chat.models import Message, Chat, MessageReadState, MuteTable, Confirm
from chat.utils import send_push
from core.constants import SEND_MESSAGE_KEY
from core.models import User, Notification, Metrics
from core.permissions import GeneralPermissionMixin, ChatPermissionMixin
from core.utils import JsonResponse, build_absolute_url, check_is_auth
from core.serializers import JsonResponseSerializer as JRS
from chat.serializers.versions.v1_1_0 import VERSION as v1, MessageSerializer
from chat.serializers.versions.v1_2_0 import VERSION as v2, ChatMessagesSerializer

logger = logging.getLogger(__name__)


class MessageObject(MessageMixin, RetrieveAPIView):
    pass


class ChatsList(ChatMixin, ListAPIView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        if 'notification_slug' in request.GET and check_is_auth(request):
            if 'user_id' in request.session:
                request.user = User.objects.get(id=request.session['user_id'])

            Notification.objects.filter(
                slug=request.GET['notification_slug'],
                user=request.user
            ).update(is_return=True)

        self.queryset = (
            Chat.
            objects.
            select_related('owner').
            prefetch_related('users').
            filter(Q(owner=request.user) | Q(users__in=[request.user])).
            distinct('id').
            order_by('-id')
        )
        return super().get(request, *args, **kwargs)

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class MessagesObjects(MessageMixin, APIView):  # versions conflict

    http_method_names = ['get']

    def get(self, request, *args, **kwargs):

        if 'notification_slug' in request.GET and check_is_auth(request):
            if 'user_id' in request.session:
                request.user = User.objects.get(id=request.session['user_id'])

            Notification.objects.filter(
                slug=request.GET['notification_slug'],
                user=request.user
            ).update(is_return=True)

        chat_pk = kwargs['pk']
        try:
            chat = Chat.objects.get(pk=chat_pk)
        except Chat.DoesNotExist:
            return Response(JRS(JsonResponse(
                status=404, msg='chat does not exist')).data)

        if not self.request.user.is_chat_member(chat_pk):
            return Response(JRS(JsonResponse(
                status=400, msg='you can not see this chat')).data)

        cnt = MessageReadState.objects.filter(user=request.user, chat=chat).update(last_read=timezone.now())
        if not cnt:
            MessageReadState.objects.create(
                user=request.user,
                chat=chat,
                last_read=timezone.now()
            )

        if request.version == v1 or request.version is None:

            self.serializer_class = MessageSerializer

            self.queryset = Message.objects.select_related('author').filter(chat=chat).order_by('id')

            serializer = MessageSerializer(
                self.queryset, **{
                    'context': {
                        'request': request
                    },
                    'many': True
                }
            )

            return Response(serializer.data)

        elif request.version == v2:

            # override
            self.get_object = lambda: chat
            self.retrieve = RetrieveAPIView.retrieve
            self.get_serializer = \
                lambda instance: ChatMessagesSerializer(instance, **{'context': {
                    'request': request
                }})

            return self.retrieve(self, request, *args, **kwargs)

        return Response(JRS(JsonResponse(
            status=400, msg='wrong api version')).data)


class TotalUnreadMessages(MessageMixin, ListAPIView):

    http_method_names = ['get']

    def get(self, request, *args, **kwargs):

        qs = MessageReadState.objects.filter(user=request.user)

        prefetch = Prefetch('chat__read_states', queryset=qs)

        read_count = (
            Message.
            objects.
            select_related('chat').
            prefetch_related(prefetch).
            filter(
                Q(chat__users__in=[request.user]) | Q(chat__owner=request.user)
            ).
            filter(
                date_create__gt=F('chat__read_states__last_read'),
                chat__read_states__user=request.user
            )
        )

        data = {
            'unread_total_count': read_count.count(),
        }
        return Response(JRS(JsonResponse(status=200, msg='ok', data=data)).data)


class ChatRemove(GeneralPermissionMixin, ChatUsersMixin, DestroyAPIView):
    http_method_names = ['delete']

    def delete(self, request, *args, **kwargs):

        chat_pk = kwargs['pk']

        try:
            chat = Chat.objects.get(pk=chat_pk)
        except Chat.DoesNotExist:
            return Response(
                JRS(JsonResponse(status=404, msg='chat does not exist')).data
            )

        if request.user == chat.owner:
            new_owner = chat.users.filter().first()
            if not new_owner:
                chat.delete()
                return Response(
                    JRS(JsonResponse(status=200, msg='ok')).data
                )

            chat.users.remove(new_owner)

            chat.owner = new_owner
            chat.save()

            send_push(
                [new_owner],
                'Congratulations! 🎉🎉🎉',
                'You are admin now of chat "{0}"'.format(chat.title),
                chat=chat
            )
        else:
            chat.users.remove(request.user)
            users = [user for user in chat.users.all()]
            send_push(
                [chat.owner] + users,
                '{0} left the chat {1}'.format(
                    request.user.first_name,
                    chat.title
                ),
                '❗ You are admin now of chat "{0}"'.format(chat.title),
                chat=chat
            )

        try:
            confirm = Confirm.objects.get(meeting=chat.meeting, user__id=request.user.id)
            if confirm:
                confirm.delete()
        except Confirm.DoesNotExist:
            logger.warning('Confirm from user id {0} for chat {1} does not exist'.format(
                request.user.id,
                chat.id
            ))

        return Response(JRS(JsonResponse(
            status=200, msg='ok')).data)


class SaveMessage(APIView):

    http_method_names = ['post']

    def post(self, request, *args, **kwargs):

        key = request.POST.get('api_key')
        message = request.POST.get('message')
        chat_slug = request.POST.get('chat_slug')
        date_create = request.POST.get('date_create')

        online_user_ids = REDIS_DB.get('{0}-{1}'.format(REDIS_UNREAD_USERS_KEY, chat_slug))

        if online_user_ids:
            online_user_ids = online_user_ids.split(',')
        else:
            online_user_ids = []

        online_user_ids.append(str(request.user.id))

        if '' in online_user_ids:
            online_user_ids.remove('')

        if key != settings.CHAT_API_KEY:
            return Response(JRS(JsonResponse(
                    status=400, msg='wrong key')).data)

        try:
            chat = Chat.objects.get(channel_slug=chat_slug)
        except Chat.DoesNotExist:
            return Response(JRS(JsonResponse(
                status=400, msg='chat does not exist')).data)

        users = User.objects.filter(
            Q(chats_with_me__channel_slug=chat_slug) | Q(my_own_chats__channel_slug=chat_slug)
        ).exclude(id__in=online_user_ids)

        if users:
            try:
                apponent = users.first()
                send_mail(
                    '[LetsGo]: From {0} to {1}'.format(request.user.first_name, apponent.first_name),
                      '{0}\nLink for {1}: {2}\nLink for {3}: {4}'.format(
                          message,
                          request.user.first_name,
                          build_absolute_url('/?token={0}#chats'.format(request.user.auth_token.key)),
                          apponent.first_name,
                          build_absolute_url('/?token={0}#chats'.format(apponent.auth_token.key)),
                      ),
                      settings.EMAIL_HOST_USER,
                      [
                          'xyets777@gmail.com',
                          'kravcov2512@gmail.com',
                          'kyupetrov@gmail.com'
                      ],
                    fail_silently=False
                )
            except Exception:
                pass

        try:
            send_push(
                users,
                '✉️ {0}'.format(request.user.first_name),
                message,
                chat=chat
            )

        except IOError as e:
            logger.error('{0}'.format(e))

        Message.objects.create(
            chat=chat,
            author=request.user,
            text=message,
            date_create=date_create
        )

        Metrics.objects.create(
            user=request.user,
            metric_key=SEND_MESSAGE_KEY,
            metric_value='{}'.format(chat.id)
        )

        MessageReadState.objects.filter(chat=chat, user__in=online_user_ids).update(last_read=timezone.now())

        return Response(JRS(JsonResponse(status=200, msg='ok')).data)


class MuteChat(GeneralPermissionMixin, MuteMixin, UpdateAPIView):

    http_method_names = ['put']

    def update(self, request, *args, **kwargs):
        try:
            chat = Chat.objects.get(pk=kwargs['pk'])
        except Chat.DoesNotExist:
            return Response(JRS(JsonResponse(status=404, msg='Chat does not exist')).data)

        is_mute = False
        if request.data.get('off'):
            is_mute = True

        if MuteTable.objects.filter(chat__id=kwargs['pk'], user=request.user).exists():
            mute = MuteTable.objects.filter(chat__pk=kwargs['pk'], user=request.user).first()
            mute.is_active = is_mute
            mute.save()
        else:
            mute = MuteTable.objects.create(
                chat=chat,
                user=request.user,
                is_active=is_mute
            )

        return Response(JRS(JsonResponse(status=200, msg='ok', data={'is_mute': mute.is_active})).data)


class ChatMembers(ChatPermissionMixin, ChatMembersMixin, ListAPIView):

    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        try:
            self.queryset = User.objects.filter(
                Q(chats_with_me__pk=kwargs['pk']) | Q(my_own_chats__pk=kwargs['pk'])).distinct('pk')
        except Chat.DoesNotExist:
            return Response(JRS(JsonResponse(status=404, msg='Chat does not exist')).data)

        return super().get(request, *args, **kwargs)
