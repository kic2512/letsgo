from chat.models import Chat, Message, MuteTable
from chat.serializers.versions.v1_1_0 import ChatSerializer, MessageSerializer
from core.models import User
from core.permissions import IsStaffOrOwner, IsChatMember
from core.serializers import UserSerializer
from core.utils import AppVersion


class ChatMixin(object):
    model = Chat
    serializer_class = ChatSerializer

    who_can_update = IsStaffOrOwner
    path_to_owner_pk = 'owner.pk'


class ChatUsersMixin(ChatMixin):
    who_can_update = IsChatMember


class MessageMixin(object):
    model = Message
    versioning_class = AppVersion


class MuteMixin(object):
    model = MuteTable
    who_can_update = IsChatMember


class ChatMembersMixin(object):
    model = User
    serializer_class = UserSerializer
