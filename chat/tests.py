import json

from django.core.urlresolvers import reverse
from django.test import TestCase

from chat.models import Chat, Message
from core.tests import MeetingMixin, create_user, check_json


class ChatTests(MeetingMixin, TestCase):
    def test_message_list(self):
        chat = Chat.objects.create(
            title='test tile',
            owner=self.test_user,
            meeting=self.test_meeting_1,
            channel_slug='qwerty'

        )

        test_text = 'lalala'

        Message.objects.create(
            chat=chat,
            author=self.test_user,
            text=test_text,
            is_read=False,
            is_received=False,
        )

        response = self.client.get(
            reverse('message-list', kwargs={'pk': chat.pk})
        )

        # TODO ADD CHECK FOR MESSAGE
        self.assertEqual(200, response.status_code)

    def test_chats_list(self):
        Chat.objects.create(
            title='test tile',
            owner=self.test_user,
            meeting=self.test_meeting_1,
            channel_slug='qwerty'

        )

        response = self.client.get(
            reverse('chats-list')
        )

        # TODO ADD CHECK FOR MESSAGE
        self.assertEqual(200, response.status_code)

    def test_delete_success(self):

        chat = Chat.objects.create(
            title='test tile',
            owner=self.test_user,
            meeting=self.test_meeting_1,
            channel_slug='qwerty'

        )

        response = self.client.delete(
            reverse('remove-chat', kwargs={'pk': chat.pk})
        )

        self.assertEqual(200, response.status_code)

    def test_mute_chat(self):
        chat = Chat.objects.create(
            title='test tile',
            owner=self.test_user,
            meeting=self.test_meeting_1,
            channel_slug='qwerty'

        )

        self.client.put(
            reverse('mute-chat', kwargs={'pk': chat.pk}),
            data=json.dumps({
                'off': True
            }),
            content_type='application/json',
        )

        response = self.client.get(
            reverse('chats-list')
        )

        found = False
        for obj in response.data:
            if obj['id'] == chat.id:
                found = True
                self.assertTrue(obj['is_mute'])

        self.assertTrue(found)

        self.client.put(
            reverse('mute-chat', kwargs={'pk': chat.pk})
        )

        response = self.client.get(
            reverse('chats-list')
        )

        found = False
        for obj in response.data:
            if obj['id'] == chat.id:
                found = True
                self.assertFalse(obj['is_mute'])

        self.assertTrue(found)

    def get_chat_members(self):
        new_user_1 = create_user('new_user_1', '0')
        new_user_2 = create_user('new_user_2', '0')
        new_user_3 = create_user('new_user_3', '0')

        chat = Chat.objects.create(
            title='test tile',
            owner=self.test_user,
            meeting=self.test_meeting_1,
            channel_slug='qwerty'

        )

        chat.users.add(new_user_1)
        chat.users.add(new_user_2)
        chat.users.add(new_user_3)

        response = self.client.get(
            reverse('chat-members', kwargs={'pk': chat.pk})
        )

        for user in response.data:
            check_json(user, ('id', 'first_name', 'about'))
