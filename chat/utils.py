import json
import logging
import requests
from django.conf import settings
from chat.models import MuteTable
from core.constants import NOTIFICATION_TYPE_PUSH
from core.models import Notification, User

logger = logging.getLogger(__name__)


def send_push(users, title, message_text, push_type='message', chat=None, tokens=None):

    if not users and not tokens:
        return

    chat_id = None
    chat_slug = None
    chat_title = None

    if tokens:
        registration_ids = {'{0}'.format(token.client_key) for token in tokens}

    else:
        mute_users = []

        if chat:
            chat_id = chat.id
            chat_slug = chat.channel_slug
            if len(users) == 1:
                chat_title = chat.get_title(for_user=users[0])
            else:
                chat_title = chat.meeting.title

            for mute in MuteTable.objects.filter(chat=chat_id, user__in=users, is_active=True):
                mute_users.append(mute.user)

        registration_ids = {'{0}'.format(user.client_key) for user in users if user not in mute_users}

    if not registration_ids:
        return

    response = requests.post(
        settings.FCM_SEND_URL,
        headers={
            'Authorization': 'key={0}'.format(settings.FCM_SERVER_KEY),
            'Content-Type': 'application/json'
        },
        data=json.dumps({
            'registration_ids': list(registration_ids),
            'data': {
                'title': title,
                'text': message_text,
                'location': {
                    'name': push_type,
                    'id': chat_id,
                    'slug': chat_slug,
                    'title': chat_title
                }
            }
        }),
    )

    if 200 <= response.status_code < 300:
        for reg in registration_ids:
            user = User.objects.filter(client_key=reg).first()

            Notification.objects.create(
                user=user,
                notification_type=NOTIFICATION_TYPE_PUSH,
                title=title,
                client_key=reg
            )
