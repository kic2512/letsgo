# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-08-20 09:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20170804_0007'),
        ('chat', '0006_auto_20170730_2023'),
    ]

    operations = [
        migrations.CreateModel(
            name='MuteTable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_active', models.BooleanField(default=True)),
                ('chat', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mute_table', to='chat.Chat')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mute_table', to='core.User')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='mutetable',
            unique_together=set([('chat', 'user')]),
        ),
    ]
