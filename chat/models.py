from core.constants import TYPE_GROUP
from core.models import User, Meeting
from django.db import models


class Confirm(models.Model):

    meeting = models.ForeignKey('core.Meeting', related_name='confirms')
    user = models.ForeignKey('core.User', related_name='confirms')

    date_create = models.DateTimeField(auto_now_add=True)
    last_modify = models.DateTimeField(auto_now=True)

    is_approved = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    is_read = models.BooleanField(default=False)

    class Meta:
        unique_together = ('meeting', 'user')


class Chat(models.Model):
    title = models.CharField(max_length=32)
    owner = models.ForeignKey(User, related_name='my_own_chats')
    users = models.ManyToManyField(User, related_name='chats_with_me')
    meeting = models.ForeignKey(Meeting, related_name='chats')
    channel_slug = models.CharField(max_length=128, unique=True, db_index=True)
    date_create = models.DateTimeField(auto_now_add=True)

    def get_title(self, for_user):
        if self.meeting.group_type == TYPE_GROUP:
            return self.title

        user = self.users.filter().first()

        if not user:
            return self.title

        if for_user == self.owner:
            return user.first_name
        else:
            return self.owner.first_name


class Message(models.Model):
    chat = models.ForeignKey(Chat, related_name='messages')
    author = models.ForeignKey(User, related_name='messages')
    text = models.TextField()
    is_read = models.BooleanField(default=False)
    is_received = models.BooleanField(default=False)
    date_create = models.DateTimeField(auto_now_add=True)


class MessageReadState(models.Model):
    chat = models.ForeignKey(Chat, related_name='read_states')
    user = models.ForeignKey(User, related_name='read_states')
    last_read = models.DateTimeField()

    class Meta:
        unique_together = ('chat', 'user')

    def get_unread_count(self):
        return Message.objects.filter(chat=self.chat, date_create__gt=self.last_read).count()


class MuteTable(models.Model):
    chat = models.ForeignKey(Chat, related_name='mute_table')
    user = models.ForeignKey(User, related_name='mute_table')
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ('chat', 'user')
