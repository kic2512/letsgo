import json
import logging

import tornadoredis
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.http import urlencode
from tornado import websocket
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.web import Application
from tornado.websocket import WebSocketClosedError
from tornadoredis.pubsub import BaseSubscriber

from application.settings.base import REDIS_DB as r
from chat.constants import REDIS_UNREAD_USERS_KEY
from chat.models import Chat
from core.models import User
from core.serializers import UserSerializer
from core.utils import build_absolute_url, reverse_full

logger = logging.getLogger('management_commands')


def handle_request(respone):
    pass


def message_seve_handler(respone):
    pass


class SubscribeManager(BaseSubscriber):
    def __init__(self, tornado_redis_client):
        super().__init__(tornado_redis_client)

    def on_message(self, msg):
        message = None
        if not msg:
            return

        date_create = timezone.now()

        try:
            if msg.kind == 'message' and msg.body:
                # Get the list of subscribers for this channel
                subscribers = list(self.subscribers[msg.channel].keys())
                unsubscribe = []
                already_sent = []
                message = json.loads(msg.body)

                if subscribers:

                    users_who_get_message = set()

                    for s in subscribers:
                        is_my = message['user']['id'] == s.user.id
                        if r.get(s.session_key):
                            data = {
                                'text': message['text'],
                                'author_name': message['user']['first_name'],
                                'avatar': message['user']['avatar']['src_small'],
                                'href': reverse_full('user-detail', kwargs={'pk': s.user.id}),
                                'is_my': is_my,
                                'date_create': str(timezone.localtime(date_create))
                            }

                            get_message = True

                            try:
                                if s.user.id not in already_sent:
                                    s.write_message(json.dumps(data, ensure_ascii=False))
                                    already_sent.append(s.user.id)

                            except WebSocketClosedError:
                                if not is_my:
                                    get_message = False
                                    unsubscribe.append(s)
                            finally:
                                if get_message:
                                    users_who_get_message.add(str(s.user.id))
                        else:
                            s.close()
                            logger.info('{0} left the chat'.format(s))
                            unsubscribe.append(s)

                    if unsubscribe:
                        for s in self.subscribers[msg.channel].copy():
                            if s in unsubscribe:
                                del self.subscribers[msg.channel][s]

                    r.set('{0}-{1}'.format(REDIS_UNREAD_USERS_KEY, msg.channel), ','.join(users_who_get_message))

            super(SubscribeManager, self).on_message(msg)

        except Exception as e:
            logger.error('Global exception')
            logger.error('{0}'.format(e))

        finally:
            if msg.kind == 'message':
                http_client = AsyncHTTPClient()
                url = build_absolute_url('{0}'.format(reverse('save_message')))

                request = HTTPRequest(
                    url,
                    method="POST",
                    body=urlencode({
                        "message": json.loads(msg.body)['text'].encode("utf-8"),
                        "api_key": settings.CHAT_API_KEY,
                        "sender_id": message['user']['id'],
                        "chat_slug": msg.channel,
                        "date_create": str(date_create)
                    }),
                    headers={
                        'Authorization': 'Token {0}'.format(message['user_token'])
                    }
                )

                http_client.fetch(request, message_seve_handler)


publisher = SubscribeManager(tornadoredis.Client())
subscriber = SubscribeManager(tornadoredis.Client())


class ChatSocketHandler(websocket.WebSocketHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.channel_name = ''  # Chat unique slug
        self.session_key = None
        self.user = None

    def get_current_user(self):
        token = self.get_argument('token')
        try:
            return User.objects.get(auth_token__key=token)
        except User.DoesNotExist:
            return None

    def check_origin(self, origin):
        return True

    def open(self, *args, **kwargs):

        self.user = self.get_current_user()
        if not self.user:
            self.close()
            return

        self.session_key = 'user-{0}'.format(self.user.id)
        r.set(self.session_key, 1)

        self.channel_name = kwargs.get('channel')

        if not self.channel_name:
            self.close()
            return

        try:
            chat = Chat.objects.get(channel_slug=self.channel_name)
        except Chat.DoesNotExist:
            logger.exception('Chat with slug: {0} does not exist'.format(self.channel_name))
            self.close()
            return

        if not self.user == chat.owner and not chat.users.filter(id=self.user.id).exists():
            logger.warning('Someone try to connect to private chat. User_id: {0}'.format(self.user.id))
            self.close()
            return

        subscriber.subscribe(self.channel_name, self)

    def on_message(self, message):
        if message:
            self.message = message
            data = {
                'text': message,
                'user': UserSerializer(self.user).data,
                'user_token': self.user.auth_token.key
            }

            print(type(message))
            print(type(UserSerializer(self.user).data))
            print(type(self.user.auth_token.key))

            publisher.publish(self.channel_name, data, callback=self.new_message_callback)

    def new_message_callback(self, result):
        pass

    def data_received(self, chunk):
        pass

    def handle_response(self, response):
        pass

    def on_close(self):
        logger.warning('close connection')
        if self.session_key:
            r.delete(self.session_key)

    def __del__(self):
        if self.session_key and r.get(self.session_key):
            r.delete(self.session_key)


application = Application([
    ('/chat/(?P<channel>\w+)/', ChatSocketHandler),
])
