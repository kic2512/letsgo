from django.db import models

from core.models import User


class Payment(models.Model):
    user = models.ForeignKey(User, related_name='payments')
    date_create = models.DateTimeField(auto_now_add=True)
    value = models.IntegerField(default=0)
    comment = models.CharField(max_length=255)
