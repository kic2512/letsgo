API
====
Список мероприятий
--------------------
`GET /meetings-list/`  
*Example request headers:*  
`GET /meetings-list/ HTTP/1.1`   
`Accept: */*`  
`Accept-Encoding: gzip, deflate`  
`Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2`  
`Connection: keep-alive`  
`Host: 80.87.201.72`  
`User-Agent: HTTPie/0.9.6`  
  
Response:
```
[
    {
        "category": 0,
        "color_status": "indifferent",
        "confirms": {},
        "coordinates": {
            "lat": 55.795105,
            "lng": 37.676239
        },
        "description": "d1",
        "group_type": 0,
        "href": "http://80.87.201.72/meeting-detail/5/",
        "id": 5,
        "meeting_date": "2017-03-28",
        "owner": {
            "about": "",
            "avatar": "http://80.87.201.72/uploads/user-photos/88.jpg",
            "birth_date": null,
            "client_key": "2017-05-02 20:14:51.757789+00:00",
            "first_name": "",
            "gender": null,
            "href": "http://80.87.201.72/user-detail/2/",
            "id": 2,
            "photos": [
                {
                    "delete_photo": "http://80.87.201.72/delete-photo/33/",
                    "photo": "http://80.87.201.72/uploads/user-photos/88.jpg",
                    "set_avatar": "http://80.87.201.72/set-avatar/33/"
                },
                {
                    "delete_photo": "http://80.87.201.72/delete-photo/28/",
                    "photo": "http://80.87.201.72/uploads/user-photos/33.jpg",
                    "set_avatar": "http://80.87.201.72/set-avatar/28/"
                },
            ]
        },
        "subway": null,
        "title": "t1"
    },
```

Для фильтрации событий в этом запросе можно передавать GET параметры   

`lat` float - широта центра окружности  
`lng` float - долгота центра окружности  
`r` int - радиус окружности в километрах, для выборки пользователей внутри области  

`query` str - фраза встречающаяся в названии или описании события  

`age_from` int - организатор события старше  
`age_to` int - организатор события младше  
`gender` int - пол организатора события (0 - Ж, 1 - М)  
`category` int - категория события (список категорий берется из ручки /meeting-types/ )  
`only_my` - если параметр присутствует, то отдаются все события пользователя
 
 `countries` - list of strings, страны интересующие для совместного путешествия
  

Добавление мероприятия
-----------------------
`POST /meetings-list/`   
*Example request headers:*  
`User-Agent: curl/7.35.0`  
`Host: 80.87.201.72`  
`Accept: */*`  
`Content-Type: application/json`  
`Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2`  
`Content-Length: xxx`  

```
{
    "title": "title",
    "description": "desc",
    "coordinates": {
        "lat": 20,
        "lng":30
    }
}
```
`curl 80.87.201.72/meetings-list/ -H "Content-Type: application/json" -H "Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2" -X POST -d '{"title": "title", "description": "desc", "coordinates": {"lat": 20, "lng":30}}'`


Редактирование мероприятия
-----------------------
`PUT /meeting-detail/<meeting_id>/`   
*Example request headers:*  
`User-Agent: curl/7.35.0`  
`Host: 80.87.201.72`  
`Accept: */*`  
`Content-Type: application/json`  
`Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2`  
`Content-Length: xxx`  

```
{
    "title": "title",
    "description": "desc",
    "coordinates": {
        "lat": 20,
        "lng":30
    }
}
```

Чтобы событие не показывалось на карте нужно отрпавить этот запрос с параметром:  
```
"is_active": false
```

http PUT 80.87.201.72/meeting-detail/31/ 'Authorization: Token 36c157032c7f70ee2ddc67272357cff0d64d892f' <<< '{"coordinates": {"lat": 1, "lng": 2}, "group_type": 0, "meeting_date": "2017-12-15", "title": "Hello"}'  -v


Создание пользователя
---------------------
`POST /api-token-auth/ HTTP/1.1`  
*Example request headers:*  
`Host: 80.87.201.72`  
`User-Agent: curl/7.51.0`  
`Accept: */*`  
`Content-Type: application/json`  
`Content-Length: 91`  
```
{
    "social_slug": "vk", 
    "external_id": 112002, 
    "token": "DJSKJDKSA", 
    "first_name": "Ilia228"
}
```

В ответ возвращается токен пользователя, который
можно использовать для следующий запросов, и ссылка
на созданный профиль пользователя

```
{
    "token":"09c6fc397cc3cf22b7056da065ee9d48fbacd680",
    "href":"http://80.87.201.72/user-detail/4/"
}
```

Изменение юзера
----------------
`PUT /user-detail/1/`  
*Example request headers:*
`User-Agent: curl/7.35.0`  
`Host: 80.87.201.72`  
`Accept: */*`  
`Content-Type: application/json`  
`Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2`  
`Content-Length: 77`  

```
{
    "username": "new_nick",
    "firs_name": "new first name",
    "about": "new about"
    "client_key": "client key for FireBase",
    "countries": [
        "Spain",
        "Italy"
    ]
}
```

```curl -H "Content-Type: application/json" -H "Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2" -X PUT -d '{"username": "new_nick", "firs_name": "new first name", "about": "new about", "client_key": "990ee"}' 80.87.201.72/user-detail/1/```  
или
```http PUT https://russian-friends.com/user-detail/1/ 'Authorization: Token 4af8d18777e96a85af6978a6133b588fd0811fe8' "client_key":990ee birth_date=2000-10-10 gender=0 first_name=Papa countries:='["Spain", "Italy"]' -v```  

Загрузка фото
-------------

http -f PUT 80.87.201.72/upload-photo/55.jpg  <  ~/Downloads/55.jpg 'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  

`PUT /upload-photo/55.jpg HTTP/1.1`  
`Accept: */*`  
`Accept-Encoding: gzip, deflate`  
`Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2`  
`Connection: keep-alive`  
`Content-Length: 27475`  
`Content-Type: application/x-www-form-urlencoded; charset=utf-8`  
`Host: 80.87.201.72`  
`User-Agent: HTTPie/0.9.8`  

*в теле запроса бинарные данные картинки*


Установка аватарки
-------------------
http PUT http://80.87.201.72/set-avatar/20/ 'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  


Удаление фото
-------------------
http DELETE http://80.87.201.72/delete-photo/20/ 'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  


Список откликов на события текущего юзера
-----------------------------------------
http http://80.87.201.72/confirms-list/   'Authorization: Token ee6d9b6dcdb03b6d7666c4cc14be644272e8c150'  

Отправить запрос на участие в событии
-------------------------------------
http POST http://80.87.201.72/meeting-confirm/<meeting_id>/   'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  


Отклонить запрос на участие
---------------------------
http PUT http://80.87.201.72/confirm-action/<confirm_id>/   'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  

is_rejected=True  

Принять участника
-------------------
http PUT http://80.87.201.72/confirm-action/<confirm_id>/   'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  
is_approved=True  


Редактирование профиля
----------------------
curl 80.87.201.72/user-detail/1/ -H "Content-Type: application/json" -H "Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2" -X PUT -d '{"username": "new_nick", "firs_name": "new first name", "about": "new about"}'  


Количество непрочитанных событий
--------------------------------
http GET http://80.87.201.72/unread-confirms/  'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  
```
{
    "data": {
        "unread": 0
    },
    "msg": "ok",
    "status": 200
}
```


Количество всех непрочитанных сообщений
--------------------------------
http GET http://80.87.201.72/total-unread/  'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  
```
{

    "data": {
        "unread_total_count": 90
    },
    "msg": "ok",
    "status": 200
}
```
Список категорий встреч
--------------------------------
http GET http://80.87.201.72/meeting-types/  'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2'  


Чаты
====

Чат создается автоматически, после одобрения заявки  
если заявка была на групповое событие юзер добавляется   
в общий чат.  

Список чатов
------------
http 80.87.201.72/chats-list/  'Authorization: Token ac67fd83a6f89343ab0002b5e0b21bc025b78c5d'  

Список сообщений в чате
-----------------------

http 80.87.201.72/message-list/1/ 'Authorization: Token ac67fd83a6f89343ab0002b5e0b21bc025b78c5d'  

Сначала нужно сделать обычный GET запрос для получения всех текущих сообщений  
в чате. Далее устанавливается вебсокет соединение для получения сообщений в режиме реального времени.  


Отключить уведомления в чате
----------------------------
Чтобы отключить уведамления дергаем урл:  
http PUT 80.87.201.72/mute-chat/<chat_id>/ 'Authorization: Token 36c157032c7f70ee2ddc67272357cff0d64d892f'   
с праметром  
```
{
    off=true
}
```  

чтобы снова включить: http PUT 80.87.201.72/mute-chat/<chat_id>/  'Authorization: Token 36c157032c7f70ee2ddc67272357cff0d64d892f'   


Удаление чата
-------------

http DELETE 80.87.201.72/remove-chat/<chat_id>/ 'Authorization: Token ac67fd83a6f89343ab0002b5e0b21bc025b78c5d'  
Удаляет чат с id=chat_id  


Подключение к вебсокету
-----------------------

Для подключения к вебсокету необходимо создать объект вебсокет соединения  
и повесить его на урл данного чата, общий вид:  
`ws://80.87.201.72/chat/<chat_slug>/?token=ee6d9b6dcdb03b6d7666c4cc14be644272e8c150`  

параметр `chat_slug` идентифицирует чат.  
в отличае от обычных запросов, запросы к чату должны содержать  
токен пользователя в GET  параметре (оказалось у вебсокетов есть ограничения на заголовки)  

Далее все как в обычной работе с сокетами, вешаем события on_open, on_message, on_close  


Установка client_id для FireBase
--------------------------------
http PUT 127.0.0.1:8080/set-client-key/  'Authorization: Token 163df7faa712e242f7e6b4d270e29401e604b9b2' client_key=qwert1234
  

Запрос списка стран
-------------------

http GET https://russian-friends.com/countries-list/
HTTP/1.1 200 OK
Allow: GET, HEAD, OPTIONS
Connection: keep-alive
Content-Type: application/json
Date: Fri, 11 Jan 2019 23:56:07 GMT
Server: nginx/1.6.2
Transfer-Encoding: chunked
Vary: Accept
X-Frame-Options: SAMEORIGIN

```[
    {
        "id": 1,
        "name": "Afghanistan"
    },
    {
        "id": 2,
        "name": "Albania"
    },
    {
        "id": 3,
        "name": "Algeria"
    },
    {
        "id": 4,
        "name": "American Samoa"
    },
    ...
]```