import logging
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.models import Meeting

SALT = 'letsgo-application'

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Meeting)
def update_meeting(sender, update_fields, created, instance, **kwargs):

    if created:
        return

    instance.chats.all().update(title=instance.title)
