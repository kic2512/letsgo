from django.db.models import Q
from django.http import Http404

from chat.models import Confirm
from core.models import User, Meeting, UserPhotos
from core.permissions import IsStaffOrOwner, SomeUser
from core.serializers import UserSerializerExtended, MeetingSerializer, PhotoSerializer, \
    ConfirmSerializer, ConfirmExtendedSerializer
import datetime


class UserMixin(object):
    model = User
    serializer_class = UserSerializerExtended
    queryset = User.objects.all()
    who_can_update = IsStaffOrOwner
    path_to_owner_pk = 'pk'

    def get_object(self):
        pk = self.kwargs.get('pk', None)
        if pk is None:
            pk = self.request.user.pk
        try:
            obj = User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise
        return obj


class UserMeetingMixin(object):

    serializer_class = MeetingSerializer
    who_can_update = IsStaffOrOwner
    path_to_owner_pk = 'owner.pk'

    def get_object(self):
        pk = self.kwargs.get('user_pk', None)
        meeting = Meeting.objects.filter(owner_id=pk, is_active=True).first()
        if meeting:
            return meeting
        else:
            raise Http404


class MeetingMixin(object):
    model = Meeting
    serializer_class = MeetingSerializer
    who_can_update = IsStaffOrOwner
    path_to_owner_pk = 'owner.pk'

    lat = None
    lng = None
    r = None

    query = None

    only_my = None

    category = None
    gender = None

    age_from = None
    age_to = None

    countries = None

    def get_queryset(self):

        if self.lat is None or self.lng is None or self.r is None:
            queryset = Meeting.objects.filter(is_active=True)
        else:
            radius = self.r * 1000

            # queryset = Meeting.objects.filter(is_active=True).extra(
            #     where=[
            #         'ST_Distance_Sphere(coordinates, ST_MakePoint({lat},{lng})) <=  {r}'.format(
            #             lat=self.lat,
            #             lng=self.lng,
            #             r=radius
            #         )
            #     ]
            # )

            queryset = Meeting.objects.filter(is_active=True)

        if self.category is not None:
            queryset = queryset.filter(category=self.category)
        if self.gender is not None:
            queryset = queryset.filter(owner__gender=self.gender)
        if self.age_from is not None and self.age_to is not None:
            birth_date_from = datetime.date.today() - datetime.timedelta(days=(self.age_to*365.25))
            birth_date_to = datetime.date.today() - datetime.timedelta(days=(self.age_from*365.25))
            queryset = queryset.filter(owner__birth_date__range=[
                birth_date_from, birth_date_to
            ])

        if self.query:
            queryset = queryset.filter(Q(title__search=self.query) | Q(description__search=self.query)).distinct()

        if self.only_my:
            queryset = queryset.filter(owner=self.request.user)

        if self.countries:
            queryset = queryset.filter(
                Q(owner__country__name__in=self.countries) | Q(owner__country__ru_name__in=self.countries))

        return queryset


class PhotoMixin(object):
    model = UserPhotos
    serializer_class = PhotoSerializer
    who_can_update = IsStaffOrOwner
    path_to_owner_pk = 'owner.pk'

    def get_queryset(self):
        return UserPhotos.objects.all()


class ConfirmMixin(object):
    model = Confirm
    queryset = Confirm.objects.all()
    serializer_class = ConfirmExtendedSerializer
    who_can_update = IsStaffOrOwner
    path_to_owner_pk = 'meeting.owner.pk'


class ConfirmBasicMixin(ConfirmMixin):
    serializer_class = ConfirmSerializer


class ClientKeyMixin(object):
    who_can_update = SomeUser
