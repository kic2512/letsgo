from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'

    def ready(self):
        from core.signals.handlers import update_meeting  # noqa
        from chat.signals.handlers import add_to_chat  # noqa