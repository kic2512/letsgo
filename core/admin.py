from django.contrib import admin

from core.models import User, Meeting, UserPhotos, SocialData, Notification, Country, UserCohort, Metrics


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'about']


@admin.register(Meeting)
class MeetingAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'meeting_date',
        'description',
        'group_type',
        'category',
        'owner',
        'is_active'
    )

    search_fields = ['title', 'description']

@admin.register(UserPhotos)
class UserPhotosAdmin(admin.ModelAdmin):
    search_fields = ['owner__first_name']

@admin.register(SocialData)
class SocialDataAdmin(admin.ModelAdmin):
    search_fields = ['user__first_name', 'token']

@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    search_fields = ['title']
    list_display = ('title', 'user', 'is_read', 'is_return', 'notification_type')

@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    pass

@admin.register(UserCohort)
class UserCohortAdmin(admin.ModelAdmin):
    pass

@admin.register(Metrics)
class MetricsAdmin(admin.ModelAdmin):
    pass
