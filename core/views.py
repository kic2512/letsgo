import logging
import re
import requests

import magic
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.db import DatabaseError, IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.utils.timezone import datetime
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, RedirectView
from raven.utils import json

from rest_framework import generics
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view
from rest_framework.exceptions import ValidationError
from rest_framework.generics import UpdateAPIView, CreateAPIView, ListAPIView, DestroyAPIView, RetrieveAPIView
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from chat.models import Confirm
from chat.utils import send_push
from core.constants import BASE_ERROR_MSG, MAX_MEETINGS, \
    MOSCOW_LAT, MOSCOW_LNG, MAX_RADIUS, MEETING_CATEGORIES, MALE, FEMALE, ALLOWABLE_FILE_FORMATS, CATEGORIES_LIST, \
    UPLOAD_PHOTO, SEND_INVITE_KEY, DECLINE_INVITE_KEY, ACCEPT_INVITE_KEY, UPDATE_PROFILE, OPEN_OTHER_PROFILE, \
    OPEN_MY_PROFILE, GET_MEETINGS_KEY, CREATE_MEETING_KEY, GET_MEETING_DETAIL_KEY

from core.exceptions import UploadException, UnAuthorizedException
from core.mixins import UserMixin, MeetingMixin, PhotoMixin, ConfirmMixin, ConfirmBasicMixin, ClientKeyMixin, \
    UserMeetingMixin
from core.models import Meeting, UserPhotos, User, UnAuthorizedUsersKeys, Notification, Country, Metrics
from core.permissions import GeneralPermissionMixin, EasyPermissionMixin
from core.serializers import JsonResponseSerializer as JRS, AuthSerializer, UserSerializer, UserPhotoSerializer, \
    CountrySerializer
from core.utils import JsonResponse, build_absolute_url, make_category_obj, check_is_auth
from PIL import Image
from io import BytesIO, StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.contrib.auth.models import update_last_login

logger = logging.getLogger(__name__)


@api_view(['GET'])
def api_root(request, format=None):
    """
    The entry endpoint of our API.
    """
    return Response({
        'users': reverse('user-list', request=request),
    })


class UserList(UserMixin, generics.ListCreateAPIView):
    http_method_names = ['get', 'post']
    pass


class UserMeeting(EasyPermissionMixin, UserMeetingMixin, generics.RetrieveDestroyAPIView):
    http_method_names = ['get']


class UserDetail(EasyPermissionMixin, UserMixin, generics.RetrieveUpdateDestroyAPIView):
    http_method_names = ['get', 'post', 'put']

    def dispatch(self, request, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        request = self.initialize_request(request, *args, **kwargs)
        self.request = request
        self.headers = self.default_response_headers  # deprecate?

        if 'pk' not in kwargs:
            kwargs['pk'] = request.user.pk

        try:
            self.initial(request, *args, **kwargs)

            # Get the appropriate handler method
            if request.method.lower() in self.http_method_names:
                handler = getattr(self, request.method.lower(),
                                  self.http_method_not_allowed)
            else:
                handler = self.http_method_not_allowed

            response = handler(request, *args, **kwargs)

        except Exception as exc:
            response = self.handle_exception(exc)

        self.response = self.finalize_response(request, response, *args, **kwargs)
        return self.response

    def get(self, request, *args, **kwargs):
        try:
            if not request.user.is_anonymous:
                if request.user.id == kwargs['pk']:
                    Metrics.objects.create(
                        user=request.user,
                        metric_key=OPEN_MY_PROFILE,
                        metric_value='1'
                    )
                else:
                    Metrics.objects.create(
                        user=request.user,
                        metric_key=OPEN_OTHER_PROFILE,
                        metric_value='{}'.format(kwargs['pk'])
                    )
            else:
                Metrics.objects.create(
                    user=None,
                    metric_key=OPEN_OTHER_PROFILE,
                    metric_value='{}'.format(kwargs['pk'])
                )

            return self.retrieve(request, *args, **kwargs)
        except User.DoesNotExist:
            return Response(JRS(JsonResponse(
                status=400, msg='User does not exist')).data)

    def put(self, request, *args, **kwargs):
        Metrics.objects.create(
            user=request.user,
            metric_key=UPDATE_PROFILE,
            metric_value='1'
        )

        try:
            if 'countries' in request.data:
                request.user.country.clear()
                countries = Country.objects.filter(
                    Q(name__in=request.data['countries']) | Q(ru_name__in=request.data['countries']))
                for country in countries:
                    request.user.country.add(country)

            return super().put(request, *args, **kwargs)
        except ValidationError as e:
            return Response(JRS(JsonResponse(status=400, msg='error', data=e.detail)).data)


class MeetingsList(EasyPermissionMixin, MeetingMixin, generics.ListCreateAPIView):
    http_method_names = ['get', 'post']

    def __init__(self):
        super().__init__()
        self.lat = None
        self.lng = None
        self.r = None

        self.age_from = None
        self.age_to = None

        self.gender = None

        self.category = None

        self.only_my = None

        self.countries = None

    def post(self, request, *args, **kwargs):

        user = request.user

        Meeting.objects.filter(owner=user).update(is_active=False)

        try:
            send_mail(
                '[LetsGo]: {0} create new meeting'.format(user.first_name),
                'New meeting was created',
                settings.EMAIL_HOST_USER,
                [
                    'xyets777@gmail.com',
                    'kravcov2512@gmail.com',
                    'kyupetrov@gmail.com'
                ],
                fail_silently=False
            )
        except Exception as e:
            logger.error(e)

        Metrics.objects.create(
            user=user,
            metric_key=CREATE_MEETING_KEY,
            metric_value='1'
        )

        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):

        metric_user = request.user if not request.user.is_anonymous else None
        Metrics.objects.create(
            user=metric_user,
            metric_key=GET_MEETINGS_KEY,
            metric_value='1'
        )

        self.query = request.query_params.get('query', None)
        self.only_my = request.query_params.get('only_my', None)

        if 'notification_slug' in request.query_params and check_is_auth(request):
            if 'user_id' in request.session:
                request.user = User.objects.get(id=request.session['user_id'])

            Notification.objects.filter(
                slug=request.query_params['notification_slug'],
                user=request.user
            ).update(is_return=True)

        if 'user_id' in self.request.session:
            request.user = User.objects.get(id=self.request.session['user_id'])
            update_last_login(None, request.user)
        elif self.request.user.is_authenticated:
            update_last_login(None, request.user)

        try:
            self.lat = float(request.query_params.get('lat'))
            self.lng = float(request.query_params.get('lng'))
            self.r = float(request.query_params.get('r'))
            if self.r > MAX_RADIUS:
                self.r = MAX_RADIUS
        except ValueError:
            self.lat = MOSCOW_LAT
            self.lng = MOSCOW_LNG
            self.r = MAX_RADIUS
        except TypeError as e:
            pass

        self.r = MAX_RADIUS

        try:
            self.age_from = int(request.query_params.get('age_from'))
            self.age_to = int(request.query_params.get('age_to'))
        except (ValueError, TypeError) as e:
            pass

        try:
            self.gender = int(request.query_params.get('gender'))
            if self.gender not in (MALE, FEMALE):
                self.gender = None
        except (ValueError, TypeError) as e:
            pass

        try:
            category = int(request.query_params.get('category'))
        except (ValueError, TypeError):
            category = None

        self.countries = request.query_params.getlist('countries')

        self.category = category if category in CATEGORIES_LIST else None
        return super().get(request, *args, **kwargs)


class MeetingDetail(EasyPermissionMixin, MeetingMixin, generics.RetrieveUpdateAPIView):
    http_method_names = ['get', 'post', 'put']

    def get(self, request, *args, **kwargs):

        metric_user = request.user if not request.user.is_anonymous else None

        Metrics.objects.create(
            user=metric_user,
            metric_key=GET_MEETING_DETAIL_KEY,
            metric_value=kwargs['pk']
        )

        return super().get(request, *args, **kwargs)


class SPAView(TemplateView):
    http_method_names = ['get']

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data()
        data['vk_app_id'] = settings.VK_APP_ID
        data['redirect_url'] = 'https://russian-friends.com/auth-confirm/'
        data['SOCIAL_AUTH_FACEBOOK_APP_KEY'] = 'AIzaSyBP5iNz_d4d14nJpcOf_iBlCdFN4niZRQk'
        data['SOCIAL_AUTH_FACEBOOK_APP_ID'] = settings.FB_APP_ID
        data['SOCIAL_AUTH_FACEBOOK_APP_SECRET'] = settings.FB_APP_SECRET
        data['FACEBOOK_EXTENDED_PERMISSIONS'] = 'email'
        data['fb_redirect_uri'] = 'https://russian-friends.com/auth-fb-confirm/'

        return data


class SPAMapView(TemplateView):
    http_method_names = ['get']

    template_name = 'main.html'

    def get(self, request, *args, **kwargs):
        try:
            if 'notification_slug' in request.GET and check_is_auth(request):
                if 'user_id' in request.session:
                    request.user = User.objects.get(id=request.session['user_id'])
                Notification.objects.filter(
                    slug=request.GET['notification_slug'],
                    user=request.user
                ).update(is_return=True)
            return super().get(request, *args, **kwargs)
        except UnAuthorizedException:
            return HttpResponseRedirect(reverse('registration'))

    def get_context_data(self, **kwargs):

        data = super().get_context_data()
        data['is_debug'] = settings.DEBUG
        data['map_api_key'] = settings.GOOGLE_MAP_KEY
        data['years'] = [i + 1970 for i in range(31)]
        data['vk_app_id'] = settings.VK_APP_ID
        data['redirect_url'] = 'https://russian-friends.com/auth-confirm/'
        data['SOCIAL_AUTH_FACEBOOK_APP_KEY'] = 'AIzaSyBP5iNz_d4d14nJpcOf_iBlCdFN4niZRQk'
        data['SOCIAL_AUTH_FACEBOOK_APP_ID'] = settings.FB_APP_ID
        data['SOCIAL_AUTH_FACEBOOK_APP_SECRET'] = settings.FB_APP_SECRET
        data['FACEBOOK_EXTENDED_PERMISSIONS'] = 'email'
        data['fb_redirect_uri'] = 'https://russian-friends.com/auth-fb-confirm/'

        if 'token' in self.request.GET:
            try:
                user = User.objects.get(auth_token__key=self.request.GET['token'])
                self.request.session['user_id'] = user.id
            except User.DoesNotExist:
                raise UnAuthorizedException
        else:
            user = User.objects.filter(id=self.request.session.get('user_id')).first()

        if user:
            token = user.auth_token
            data['token'] = token.key
            data['is_time_to_pay'] = UserSerializer(user).data['its_time_to_pay']
            data['urls'] = {
                'profile': UserSerializer().get_href(user),
            }
        else:
            data['token'] = None
            data['is_time_to_pay'] = None
            data['urls'] = {
                'profile': None,
            }

        return data


class AuthFBConfirm(RedirectView):
    url = '/#user-settings'

    def get(self, request, *args, **kwargs):
        token_response = json.loads(requests.get(url='https://graph.facebook.com/v3.0/oauth/access_token', params={
            'client_id': settings.FB_APP_ID,
            'redirect_uri': 'https://russian-friends.com/auth-fb-confirm/',
            'client_secret': settings.FB_APP_SECRET,
            'code': request.GET.get('code')
        }).text)

        if 'access_token' not in token_response:
            logger.error('token_response: {0}'.format(token_response))
            self.url = '/'
            return super().get(request, *args, **kwargs)

        user_response = json.loads(requests.get('https://graph.facebook.com/me?', {
            'fields': 'id,email,first_name,gender',
            'access_token': token_response['access_token']
        }).text)

        logger.error(user_response)

        serializer_data = {
            'social_slug': 'fb',
            'external_id': user_response['id'],
            'token': token_response['access_token'],
            'first_name': user_response['first_name'],
            'email': user_response.get('email', ''),
            'from_web': True
        }

        serializer = AuthSerializer(data=serializer_data)

        try:
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data['user']

        except ValidationError as e:
            logger.error('Validation error: {0}'.format(e))
        except AssertionError as e:
            logger.error('Serialize error: {0}'.format(e))
        else:
            user.save()
            self.request.user = user
            self.request.session['user_id'] = user.id
            token, _ = Token.objects.get_or_create(user=user)
            return super().get(request, *args, **kwargs)

        self.url = '/'
        return super().get(request, *args, **kwargs)


class AuthGoogleConfirm(RedirectView):
    http_method_names = ['post']
    url = '/'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):  # TODO check POST data by google api

        name = request.POST.get('first_name')
        external_id = request.POST.get('external_id')
        email = request.POST.get('email', '')

        serializer_data = {
            'social_slug': 'google',
            'external_id': external_id,
            'token': external_id,
            'first_name': name,
            'email': email,
            'from_web': True
        }

        serializer = AuthSerializer(data=serializer_data)

        try:
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data['user']

        except ValidationError as e:
            logger.error('Validation error: {0}'.format(e))
            self.url = reverse('error-page')
        except AssertionError as e:
            logger.error('Serialize error: {0}'.format(e))
            self.url = reverse('error-page')
        else:
            user.save()
            self.request.user = user
            self.request.session['user_id'] = user.id
            token, _ = Token.objects.get_or_create(user=user)

        return super().post(request, *args, **kwargs)


class AuthConfirm(RedirectView):
    url = '/#user-settings'

    def get(self, request, *args, **kwargs):
        '''
        https://oauth.vk.com/access_token?
        client_id=APP_ID&
        client_secret=APP_SECRET&
        code=7a6fa4dff77a228eeda56603b8f53806c883f011c40b72630bb50df056f6479e52a&
        redirect_uri=REDIRECT_URI
        '''

        response = requests.get('https://oauth.vk.com/access_token', {
            'client_id': settings.VK_APP_ID,
            'client_secret': settings.VK_APP_SECRET,
            'code': request.GET.get('code', ''),
            'redirect_uri': 'https://russian-friends.com/auth-confirm/',
        })

        if response.status_code == 200:
            response_data = json.loads(response.text)

            if 'access_token' not in response_data:
                self.url = '/'
                return super().get(request, *args, **kwargs)

            user_response = json.loads(requests.get('https://api.vk.com/method/users.get', {
                'user_ids': response_data['user_id'],
                'access_token': response_data['access_token'],
                'v': '5.78'
            }).text)['response'][0]

            logger.error(user_response)

            serializer_data = {
                'social_slug': 'vk',
                'external_id': response_data['user_id'],
                'token': response_data['access_token'],
                'first_name': user_response['first_name'],
                'email': user_response.get('email', ''),
                'from_web': True
            }

            serializer = AuthSerializer(data=serializer_data)

            try:
                serializer.is_valid(raise_exception=True)
                user = serializer.validated_data['user']

            except ValidationError as e:
                logger.error('Validation error: {0}'.format(e))
            except AssertionError as e:
                logger.error('Serialize error: {0}'.format(e))
            else:
                user.save()
                self.request.user = user
                self.request.session['user_id'] = user.id
                token, _ = Token.objects.get_or_create(user=user)
                return super().get(request, *args, **kwargs)

            self.url = '/'
            return super().get(request, *args, **kwargs)
        else:
            logger.error(response)
            return HttpResponseRedirect('/')


class ErrorTemplate(TemplateView):
    template_name = 'error_page.html'


class AuthView(ObtainAuthToken):
    serializer_class = AuthSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        return Response({
            'token': token.key,
            'href': build_absolute_url(reverse('user-detail', kwargs={'pk': user.pk}))
        })


class FileUploadView(APIView):
    parser_classes = (FileUploadParser,)

    url_prefix = 'user-photos'

    storage = default_storage

    view_context = {}

    def crop_image(self, filename, file_obj):
        try:
            file_obj.seek(0)
            image = Image.open(BytesIO(file_obj.read()))
            size = image.size
            if size[0] == size[1]:
                return file_obj
            min_size = min(size)
            image = image.crop((0, 0, min_size, min_size))
            image_io = BytesIO()
            image.save(image_io, format='JPEG')
            image_io.seek(0)
            file = InMemoryUploadedFile(image_io, None, 'foo.jpg', 'image/jpeg',
                                        image_io.getbuffer().nbytes, None)

        except IOError as e:
            return Response(JRS(JsonResponse(status=400,
                                             msg='something wrong with reading file')).data)

        return file

    def validate_request(self):
        if 'file' not in self.request.data:
            raise UploadException(response=JsonResponse(status=400, msg='error: no file in request'))

    def check_mime_type(self, file_obj):

        mime_type = magic.from_file(file_obj.name, mime=True)
        if not re.match('image/', mime_type):
            raise UploadException(
                response=JsonResponse(status=400, msg='error wrong file mime type: "{}"'.format(mime_type)))
        img_format = mime_type.split('/')[1]
        if img_format not in ALLOWABLE_FILE_FORMATS:
            raise UploadException(
                response=JsonResponse(status=400, msg='error wrong img format: "{}"'.format(img_format)))

    def save_img_size(self, filename, file_obj, resize=None):

        if resize:
            first_part = '{0}x{1}'.format(resize[0], resize[1])
            try:
                file_obj.seek(0)
                image = Image.open(file_obj)
                tmpfile = image.resize(resize, Image.NEAREST)
                tmpfile_io = BytesIO()
                tmpfile.save(tmpfile_io, format='JPEG')
                file_obj = ContentFile(tmpfile_io.getvalue())
            except OSError as e:
                file_obj.seek(0)
                content = BytesIO(file_obj.read())
                image = Image.open(content)
                logger.error(e)
                first_part = 'broken'
        else:
            first_part = 'original'

        local_path = '{0}/{1}_{2}'.format(self.url_prefix, first_part, filename)

        return '{0}{1}'.format(settings.MEDIA_URL, self.storage.save(local_path, file_obj))

    def save_file(self, filename, file_obj):

        full_path_original = self.save_img_size(filename, file_obj)
        full_path_small = self.save_img_size(filename, file_obj, (150, 150))
        full_path_medium = self.save_img_size(filename, file_obj, (600, 600))

        try:
            photos_cnt = UserPhotos.objects.filter(owner=self.request.user).count()
            if photos_cnt > 0:
                photo = UserPhotos.objects.create(
                    owner=self.request.user,
                    src_original=full_path_original,
                    src_small=full_path_small,
                    src_medium=full_path_medium,
                )
            else:
                photo = UserPhotos.objects.create(
                    owner=self.request.user,
                    src_original=full_path_original,
                    src_small=full_path_small,
                    src_medium=full_path_medium,
                    is_avatar=True
                )
        except DatabaseError as e:
            logger.error(
                'Can not save photo for user_id={0}, photo_path: {1}\nError:{2}'.format(
                    self.request.user.id,
                    self.view_context['path'],
                    e
                )
            )
        else:
            self.view_context['photo_obj'] = photo

    def put(self, request, filename, format=None):
        self.request = request
        try:
            Metrics.objects.create(
                user=request.user,
                metric_key=UPLOAD_PHOTO,
                metric_value='1',
            )
            self.validate_request()
            file_obj = request.data['file']
            # TODO FIX
            # self.check_mime_type(file_obj)
            # image_file = self.crop_image(filename, file_obj)
            self.save_file(filename, file_obj)

        except UploadException as e:
            return Response(JRS(e.response).data)

        data = UserPhotoSerializer(self.view_context['photo_obj']).data

        return Response(
            JRS(
                JsonResponse(
                    status=204,
                    msg='ok',
                    data=data
                )
            ).data
        )


class DeletePhoto(GeneralPermissionMixin, PhotoMixin, DestroyAPIView):
    parser_classes = (FileUploadParser,)
    url_prefix = 'user-photos'
    storage = default_storage

    def delete(self, request, *args, **kwargs):
        id = kwargs['pk']
        file_path = None
        try:
            target_photo = UserPhotos.objects.get(owner=self.request.user, id=id)
            file_path = target_photo.src_original
            UserPhotos.objects.filter(owner=self.request.user, id=id).delete()

        except OSError as e:
            logger.error(
                'Path to file does not exist file={0}\nError: {1}'.format(file_path, e))
            return Response(JRS(JsonResponse(status=400, msg='Path to file does not exist')).data)

        except UserPhotos.DoesNotExist as e:
            logger.error(
                'User does not exists user_id={0}, photo_id: {1}\nError: {2}'.format(self.request.user.id, id, e))
            return Response(JRS(JsonResponse(status=400, msg='user does not exists')).data)

        except UserPhotos.MultipleObjectsReturned as e:
            logger.error(
                'Duplicate key for user_id={0}, photo_id: {1}\nError: {2}'.format(self.request.user.id, id, e))
            return Response(JRS(JsonResponse(status=500, msg=BASE_ERROR_MSG)).data)

        except DatabaseError as e:
            logger.error(
                'Can not set avatar for user_id={0}, photo_id: {1}\nError: {2}'.format(self.request.user.id, id, e))
            return Response(JRS(JsonResponse(status=500, msg=BASE_ERROR_MSG)).data)

        return Response(JRS(JsonResponse(status=204, msg='ok')).data)


class SetAvatar(GeneralPermissionMixin, PhotoMixin, UpdateAPIView):
    http_method_names = ['put']

    def put(self, request, *args, **kwargs):
        obj_pk = kwargs['pk']
        try:
            UserPhotos.objects.filter(owner=self.request.user, is_avatar=True).update(is_avatar=False)

            obj = UserPhotos.objects.get(pk=obj_pk, owner=self.request.user)
            obj.is_avatar = True
            obj.save()

        except UserPhotos.DoesNotExist as e:
            logger.error(
                'User does not exists user_id={0}, photo_id: {1}\nError: {2}'.format(self.request.user.id, obj_pk, e))
            return Response(JRS(JsonResponse(status=400, msg='user does not exists')).data)

        except UserPhotos.MultipleObjectsReturned as e:
            logger.error(
                'Duplicate key for user_id={0}, photo_id: {1}\nError: {2}'.format(self.request.user.id, obj_pk, e))
            return Response(JRS(JsonResponse(status=500, msg=BASE_ERROR_MSG)).data)

        except DatabaseError as e:
            logger.error(
                'Can not set avatar for user_id={0}, photo_id: {1}\nError: {2}'.format(self.request.user.id, obj_pk, e))
            return Response(JRS(JsonResponse(status=500, msg=BASE_ERROR_MSG)).data)

        return Response(JRS(JsonResponse(status=200, msg='ok')).data)


class ConfirmCreate(GeneralPermissionMixin, CreateAPIView):
    http_method_names = ['post']

    def create(self, request, *args, **kwargs):

        meeting_pk = kwargs['pk']
        try:
            meeting = Meeting.objects.get(pk=meeting_pk)
        except Meeting.DoesNotExist:
            return Response(JRS(JsonResponse(status=404, msg='meeting does not exist')).data)

        check_confirm = Confirm.objects.filter(meeting_id=meeting_pk, user=request.user).exists()
        if check_confirm:
            return Response(JRS(JsonResponse(status=400, msg='you cannot create more than one confirm')).data)

        if meeting.owner_id == request.user.id:
            return Response(JRS(JsonResponse(status=400, msg='you can not confirm to your event')).data)

        Confirm.objects.create(meeting=meeting, user=request.user)

        Metrics.objects.create(
            user=request.user,
            metric_key=SEND_INVITE_KEY,
            metric_value='1',
        )

        try:

            send_mail('[LetsGo]: From {0} to {1}'.format(request.user.first_name, meeting.owner.first_name),
                      'Create request to chat!\n Link for {0}: {1}\nLink for {2}: {3}'.format(
                          request.user.first_name,
                          build_absolute_url('/?token={0}#requests'.format(request.user.auth_token.key)),
                          meeting.owner.first_name,
                          build_absolute_url('/?token={0}#requests'.format(meeting.owner.auth_token.key))
                      ),
                      settings.EMAIL_HOST_USER,
                      [
                          'xyets777@gmail.com',
                          'kravcov2512@gmail.com',
                          'kyupetrov@gmail.com'
                      ],
                      fail_silently=False)

        except Exception as e:
            logger.error(e)

        send_push(
            [meeting.owner],
            request.user.first_name,
            '⁉️ Want to chats with you!',
            push_type='confirms'
        )

        return Response(JRS(JsonResponse(status=200, msg='ok')).data)


class ConfirmsList(GeneralPermissionMixin, ConfirmMixin, ListAPIView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):

        if 'notification_slug' in request.GET and check_is_auth(request):
            if 'user_id' in request.session:
                request.user = User.objects.get(id=request.session['user_id'])
            Notification.objects.filter(
                slug=request.GET['notification_slug'],
                user=request.user
            ).update(is_return=True)

        self.queryset = Confirm.objects.filter(
            meeting__owner=request.user,
            is_approved=False,
            is_rejected=False
        )

        Confirm.objects.filter(meeting__owner=request.user).update(is_read=True)

        return super().get(request, *args, **kwargs)


class AcceptConfirm(GeneralPermissionMixin, ConfirmBasicMixin, UpdateAPIView):
    http_method_names = ['put']

    def update(self, request, *args, **kwargs):
        if 'is_rejected' in request.data:
            if request.data['is_rejected']:
                Metrics.objects.create(
                    user=request.user,
                    metric_key=DECLINE_INVITE_KEY,
                    metric_value='1'
                )

        elif 'is_approved' in request.data:
            if request.data['is_approved']:
                Metrics.objects.create(
                    user=request.user,
                    metric_key=ACCEPT_INVITE_KEY,
                    metric_value='1'
                )
        return super().update(request, *args, **kwargs)


class MeetingTypes(EasyPermissionMixin, ListAPIView):
    def get(self, request, *args, **kwargs):
        answer = []
        for k, v in MEETING_CATEGORIES.items():
            answer.append(
                make_category_obj(k, v[0], v[1])
            )
        return Response(JRS(JsonResponse(status=200, msg='ok', data=answer)).data)


class UnreadConfirms(GeneralPermissionMixin, ListAPIView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        count = Confirm.objects.filter(meeting__owner=request.user, is_read=False).count()
        answer = {"unread": count}
        return Response(JRS(JsonResponse(status=200, msg='ok', data=answer)).data)


class UpdateClientKey(EasyPermissionMixin, ClientKeyMixin, APIView):
    http_method_names = ['put', 'post']

    def post(self, request, *args, **kwargs):
        try:
            UnAuthorizedUsersKeys.objects.create(client_key=self.request.data['client_key'])
            return Response(
                JRS(JsonResponse(status=200, msg='ok')).data
            )
        except IntegrityError as e:
            logger.error(e)
            return Response(
                JRS(JsonResponse(status=400, msg='error', data={'error': 'duplicate field - `client_key`'})).data
            )

    def put(self, request, *args, **kwargs):
        if 'client_key' in self.request.data:
            if request.user.is_authenticated:
                self.request.user.client_key = self.request.data['client_key']
                self.request.user.save()
                UnAuthorizedUsersKeys.objects.filter(client_key=self.request.data['client_key']).delete()
            else:
                return self.post(request, *args, **kwargs)

            return Response(JRS(JsonResponse(status=200, msg='ok', data=UserSerializer(self.request.user).data)).data)
        else:
            return Response(JRS(JsonResponse(status=400, msg='error', data={
                'error: it is need to provide `client_key` param'})).data)


class ApplePayView(APIView):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        apple_url = request.POST.get('url')
        apple_response = requests.post(apple_url, {
            'merchantIdentifier': 'merchant.russian-friends.com',
            'domainName': 'russian-friends.com',
            'displayName': 'ID for letsGO web'
        })

        logger.error('[APPLE PAY REQUEST]')
        logger.error(apple_response.text)

        return Response(JRS(JsonResponse(status=501, msg='not ok', data={'error': 'method not realized'})))


class MailPreview(TemplateView):
    template_name = 'letters/test.html'


class NotificationRead(EasyPermissionMixin, RetrieveAPIView):
    def get(self, request, *args, **kwargs):
        if Notification.objects.filter(user_id=kwargs['user_id'], slug=kwargs['notification_slug']).exists():
            Notification.objects.filter(user_id=kwargs['user_id'], slug=kwargs['notification_slug']).update(is_read=True)

        return Response(JRS(JsonResponse(status=200, msg='ok', data={'status': 'success'})).data)


class CountriesList(EasyPermissionMixin, ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
