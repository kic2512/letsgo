from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q

from core.constants import DEFAULT_AVATAR, TYPE_ONE_BY_ONE
from core.utils import build_absolute_url
from location.models import Subway
from django.contrib.gis.db import models as gis_models
from django.apps import apps
from django.utils.translation import ugettext_lazy as _

class UserCohort(models.Model):
    date_create = models.DateTimeField(auto_now_add=True)
    description = models.TextField()

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'Когорта пользователей'
        verbose_name_plural = 'Когорты пользователей'


class User(AbstractUser):
    about = models.CharField(max_length=256, blank=True)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ('email',)
    birth_date = models.DateField(null=True, blank=True)
    gender = models.SmallIntegerField(null=True, blank=False)
    client_key = models.CharField(max_length=256, null=False, blank=True)
    email = models.EmailField(_('email address'), null=True, blank=True)
    from_web = models.BooleanField(default=False, blank=True)
    is_fake = models.BooleanField(default=False, blank=True)
    country = models.ManyToManyField('Country', related_name='users')
    cohort = models.ForeignKey(UserCohort, related_name='users', null=True)

    def get_avatar(self):

        obj = self.photos.filter(is_avatar=True).first()

        if not obj:
            obj = self.photos.filter().first()

        if not obj or not obj.src_original:
            return {
                'src_small': build_absolute_url(DEFAULT_AVATAR),
                'src_medium': build_absolute_url(DEFAULT_AVATAR)
            }

        return {
            'src_small': build_absolute_url(obj.src_small),
            'src_medium': build_absolute_url(obj.src_medium)
        }

    def is_chat_member(self, chat_id):
        chat_model = apps.get_model('chat', 'Chat')
        return chat_model.objects.filter(id=chat_id).filter(Q(users__in=[self]) | Q(owner=self)).exists()

    def __str__(self):
        return self.first_name

    class Meta:
        verbose_name = 'Юзер'
        verbose_name_plural = 'Юзеры'


class Meeting(models.Model):
    title = models.CharField(max_length=32)
    description = models.TextField(blank=True)
    owner = models.ForeignKey(User, related_name='created_meetings')
    subway = models.ForeignKey(Subway, null=True, blank=True, related_name='meetings')
    coordinates = gis_models.PointField(null=True, blank=False)
    date_create = models.DateTimeField(auto_now_add=True)
    meeting_date = models.DateField(null=True, blank=True)
    last_modify = models.DateTimeField(auto_now=True)
    group_type = models.SmallIntegerField(default=TYPE_ONE_BY_ONE, blank=True)
    category = models.SmallIntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'События'


class UserPhotos(models.Model):
    owner = models.ForeignKey(User, related_name='photos')
    src_original = models.URLField(null=True, blank=True)
    src_small = models.URLField(null=True, blank=True)
    src_medium = models.URLField(null=True, blank=True)
    is_avatar = models.BooleanField(default=False)

    def __str__(self):
        return self.owner.first_name


    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фотки'
        ordering = ['-is_avatar']


class SocialData(models.Model):
    user = models.OneToOneField(User)
    social_slug = models.CharField(max_length=16)
    external_id = models.PositiveIntegerField()
    token = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.user.first_name

    class Meta:
        verbose_name = 'Соц.сети'
        verbose_name_plural = 'Соц.сети'
        unique_together = ('social_slug', 'external_id')


class UnAuthorizedUsersKeys(models.Model):
    client_key = models.CharField(max_length=256, null=False, blank=False, unique=True)
    date_create = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)


class Notification(models.Model):
    user = models.ForeignKey(User, related_name='notifications', null=True, blank=True)
    date_create = models.DateTimeField(auto_now_add=True)
    notification_type = models.IntegerField()
    slug = models.CharField(max_length=256)
    title = models.CharField(max_length=512)
    client_key = models.CharField(max_length=256, blank=True)
    is_read = models.BooleanField(default=False)
    is_return = models.BooleanField(default=False)


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Уведомление'
        verbose_name_plural = 'Уведомления'


class Country(models.Model):
    name = models.CharField(max_length=64)
    ru_name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'
        ordering = ['name']


class Metrics(models.Model):
    user = models.ForeignKey(User, related_name='metrics', null=True)
    metric_key = models.CharField(max_length=32)
    metric_value = models.CharField(max_length=32)
    date_create = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.metric_key

    class Meta:
        verbose_name = 'Метрика'
        verbose_name_plural = 'Метрики'
