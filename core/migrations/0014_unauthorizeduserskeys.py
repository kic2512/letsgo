# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-07-11 08:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20180706_0001'),
    ]

    operations = [
        migrations.CreateModel(
            name='UnAuthorizedUsersKeys',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client_key', models.CharField(max_length=256, unique=True)),
                ('date_create', models.DateTimeField(auto_now_add=True)),
                ('last_update', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
