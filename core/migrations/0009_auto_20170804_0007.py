# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-08-03 21:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_user_client_key'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userphotos',
            old_name='photo',
            new_name='src_original',
        ),
        migrations.AddField(
            model_name='userphotos',
            name='src_medium',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='userphotos',
            name='src_small',
            field=models.URLField(blank=True, null=True),
        ),
    ]
