# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-11-02 12:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20170826_1244'),
    ]

    operations = [
        migrations.AddField(
            model_name='meeting',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
