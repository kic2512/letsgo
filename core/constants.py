
BASE_ERROR_MSG = 'Ooops, sorry we have a little problem, please try again later'

MAX_MEETINGS = 3

MOSCOW_LAT = 55.751244
MOSCOW_LNG = 37.618423

PITER_LAT = 59.934280
PITER_LNG = 30.335099

MAX_RADIUS = 500

DAYS_IN_YEAR = 365.25

MINE = 'mine'
APPROVED = 'approved'
AWAITING = 'awaiting'
DISAPPROVED = 'disapproved'
INDIFFERENT = 'indifferent'

FEMALE = 0
MALE = 1

GAMES = 1
CULTURE = 2
NIGHT = 3
EDUCATION = 4
WALK = 5
TRAVEL = 6
JOB = 7
SPORT = 8
DANCE = 9
SHOPPING = 10
GENERAL = 0

CATEGORIES_LIST = [GAMES, CULTURE, NIGHT, EDUCATION, WALK, TRAVEL, JOB, SPORT, DANCE, SHOPPING, GENERAL]

MEETING_CATEGORIES = {
    GAMES: ('games', 'игры'),
    CULTURE: ('culture', 'культура'),
    NIGHT: ('night', 'ночная жизнь'),
    EDUCATION: ('education', 'образование'),
    WALK: ('walk', 'прогулки'),
    TRAVEL: ('travel', 'путешествия'),
    JOB: ('job', 'работа'),
    SPORT: ('sport', 'спорт'),
    DANCE: ('dance', 'танцы'),
    SHOPPING: ('shopping', 'шопинг'),
    GENERAL: ('general', 'общее'),
}

ALLOWABLE_FILE_FORMATS = [
    "png",
    "jpeg"
]

TYPE_ONE_BY_ONE = 0
TYPE_GROUP = 1

DEFAULT_AVATAR = '/uploads/user-photos/default.jpeg'

NOTIFICATION_TYPE_EMAIL = 0
NOTIFICATION_TYPE_PUSH = 1

# METRICS KEYS
VISIT_KEY = 0
SEND_INVITE_KEY = 1
ACCEPT_INVITE_KEY = 2
SEND_MESSAGE_KEY = 3
OPEN_MY_PROFILE = 4
GET_MEETINGS_KEY = 5
SAW_AD_KEY = 6
UPLOAD_PHOTO = 7
UPDATE_PROFILE = 8
DECLINE_INVITE_KEY = 9
OPEN_OTHER_PROFILE = 10
CREATE_MEETING_KEY = 11
GET_MEETING_DETAIL_KEY = 12
