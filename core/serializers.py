import random
from collections import OrderedDict

import datetime

import requests
from django.conf import settings
from django.contrib.gis.geos import Point
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import raise_errors_on_nested_writes
from rest_framework.settings import api_settings
from django.core.urlresolvers import reverse
from core.constants import MINE, APPROVED, DISAPPROVED, AWAITING, INDIFFERENT, MEETING_CATEGORIES, DAYS_IN_YEAR, FEMALE, \
    TYPE_ONE_BY_ONE

from chat.models import Confirm, Chat
from core.models import User, Meeting, UserPhotos, SocialData, Country
from core.utils import reverse_full, build_absolute_url, make_category_obj
import hashlib
import logging

logger = logging.getLogger(__name__)


class SmartUpdaterMixin(object):

    UPDATE_AVAILABLE_FIELDS = tuple()

    def update(self, instance, validated_data):
        raise_errors_on_nested_writes('update', self, validated_data)

        for attr, value in validated_data.items():
            if attr in self.UPDATE_AVAILABLE_FIELDS:
                setattr(instance, attr, value)

        instance.save()

        return instance


class UserSerializer(SmartUpdaterMixin, serializers.ModelSerializer):

    UPDATE_AVAILABLE_FIELDS = ('first_name', 'about', 'gender', 'birth_date', 'client_key')
    avatar = SerializerMethodField()
    href = SerializerMethodField(read_only=True)
    birth_date = serializers.DateField(required=False)
    gender = serializers.IntegerField(required=True)
    age = serializers.SerializerMethodField(read_only=True)
    its_time_to_pay = serializers.SerializerMethodField(read_only=True)
    countries = serializers.SerializerMethodField(read_only=True)

    def get_age(self, obj):
        if obj.birth_date:
            birth_datetime = datetime.datetime.combine(obj.birth_date, datetime.datetime.min.time())
            return int((datetime.datetime.now() - birth_datetime).days / DAYS_IN_YEAR)
        else:
            return None

    def get_avatar(self, obj):
        return obj.get_avatar()

    def get_countries(self, obj):
        if 'request' in self.context:
            lang = self.context['request'].GET.get('lang')
        else:
            lang = 'en'

        if lang == 'ru':
            return list(obj.country.values_list('ru_name', flat=True))
        else:
            return list(obj.country.values_list('name', flat=True))

    def get_href(self, obj):
        return reverse_full('user-detail', kwargs={'pk': obj.id})

    def get_its_time_to_pay(self, obj):

        if obj.payments.first():
            return False

        first_message = obj.messages.order_by('id').first()

        if not first_message:
            return False

        if obj.gender == FEMALE:
            return False

        first_message_date = first_message.date_create
        current_date = datetime.datetime.now(datetime.timezone.utc)
        date_diff = current_date - first_message_date

        return False
        #return True if date_diff.days >= 2 else False


    class Meta:
        model = User
        fields = (
            'id',
            'age',
            'gender',
            'birth_date',
            'first_name',
            'about',
            'password',
            'avatar',
            'href',
            'client_key',
            'its_time_to_pay',
            'countries',
        )

        extra_kwargs = {
            'password': {'write_only': True, 'required': False},
            'avatar': {'required': False},
            'client_key': {'required': False},
            'photos': {'required': False},
            'birth_date': {'required': False},
        }

    def create(self, validated_data):
        user = User.objects.create(username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()

        return user


class UserPhotoSerializer(serializers.ModelSerializer):
    photo = serializers.SerializerMethodField()
    delete_photo = serializers.SerializerMethodField()
    set_avatar = serializers.SerializerMethodField()

    src_small = serializers.SerializerMethodField()
    src_medium = serializers.SerializerMethodField()
    src_original = serializers.SerializerMethodField()

    def get_delete_photo(self, obj):
        result = reverse('delete-photo', kwargs={'pk': obj.id})
        return build_absolute_url(result)

    def get_set_avatar(self, obj):
        result = reverse('set-avatar', kwargs={'pk': obj.id})
        return build_absolute_url(result)

    def get_photo(self, obj):
        return build_absolute_url(obj.src_original)

    def get_src_small(self, obj):
        return build_absolute_url(obj.src_small)

    def get_src_medium(self, obj):
        return build_absolute_url(obj.src_medium)

    def get_src_original(self, obj):
        return build_absolute_url(obj.src_original)

    class Meta:
        model = UserPhotos
        fields = ('photo', 'delete_photo', 'set_avatar', 'src_small', 'src_medium', 'src_original')


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPhotos
        fields = ('is_avatar', )


class UserSerializerExtended(UserSerializer):
    photos = UserPhotoSerializer(many=True, required=False, read_only=True)
    countries = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = (
            'id',
            'age',
            'first_name',
            'about',
            'birth_date',
            'gender',
            'password',
            'avatar',
            'photos',
            'href',
            'client_key',
            'its_time_to_pay',
            'countries',
        )

        extra_kwargs = {
            'password': {'write_only': True, 'required': False},
            'avatar': {'required': False},
            'client_key': {'required': False},
            'photos': {'required': False},
        }


class LocationSerializer(serializers.Field):

    def validate_coordinates(self, coordinates):
        if not isinstance(coordinates, dict):
            raise ValidationError('Invalid request data: coordinates must be an object instance')

        if 'lat' not in coordinates or 'lng' not in coordinates:
            raise ValidationError('Invalid request data: coordinates must contains "lat" and "lng" values')

        try:
            float(coordinates['lat'])
            float(coordinates['lng'])
        except (ValueError, TypeError):
            raise ValidationError('Invalid request data: coordinates must contains "lat" and "lng" values')

    def to_internal_value(self, data):

        if not isinstance(data, dict):
            message = self.error_messages['invalid'].format(
                datatype=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            })

        errors = OrderedDict()

        try:
            self.validate_coordinates(data)
        except ValidationError as e:
            errors['coordinates'] = e.detail

        return Point(float(data['lat']), float(data['lng']))

    def to_representation(self, instance):
        return {
            'lat': instance.coords[0],
            'lng': instance.coords[1],
        }


class ConfirmSerializer(SmartUpdaterMixin, serializers.ModelSerializer):

    UPDATE_AVAILABLE_FIELDS = ('is_approved', 'is_rejected', 'is_read')

    user = UserSerializer(read_only=True)

    class Meta:
        model = Confirm
        fields = ('id', 'user', 'date_create', 'is_approved', 'is_rejected', 'is_read')


class ChatShortSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField()
    channel_slug = serializers.CharField()

    def get_title(self, obj):
        return obj.get_title(self.context['request'].user)

    class Meta:
        model = Chat
        fields = ('id', 'channel_slug', 'title')


class MeetingSerializer(SmartUpdaterMixin, serializers.ModelSerializer):

    UPDATE_AVAILABLE_FIELDS = ('title', 'description', 'coordinates', 'meeting_date', 'category', 'is_active')

    owner = UserSerializerExtended(required=False)

    coordinates = LocationSerializer(read_only=False)

    href = serializers.SerializerMethodField()

    confirms = ConfirmSerializer(required=False)

    meeting_date = serializers.DateTimeField(required=False)  # TODO REMOVE

    color_status = serializers.SerializerMethodField()

    group_type = serializers.IntegerField(required=False)

    category = serializers.IntegerField(required=False)
    category_obj = serializers.SerializerMethodField()

    chats = ChatShortSerializer(required=False, many=True)

    is_active = serializers.BooleanField(required=False)

    def get_category_obj(self, obj):
        category_id = obj.category
        return make_category_obj(category_id, MEETING_CATEGORIES[category_id][0], MEETING_CATEGORIES[category_id][1])

    def get_color_status(self, obj):

        user_id = self.context['request'].session.get('user_id')
        request_user = None

        if not user_id:
            try:
                request_user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                if not self.context['request'].user.is_anonymous:
                    request_user = self.context['request'].user

        if not request_user:
            return INDIFFERENT

        if request_user.id == obj.owner.id:
            return MINE

        my_confirm = Confirm.objects.filter(
            meeting=obj,
            user=request_user
        ).first()

        if not my_confirm:
            return INDIFFERENT

        if my_confirm.is_approved and not my_confirm.is_rejected:
            return APPROVED

        if my_confirm.is_rejected and not my_confirm.is_approved:
            return DISAPPROVED

        return AWAITING

    def get_href(self, obj):
        return reverse_full('meeting-detail', kwargs={'pk': obj.id})

    def serialize_coordinates(self, instance):
        return {
            'lat': instance.coordinates.coords[0],
            'lng': instance.coordinates.coords[1],
        }

    def create(self, validated_data):
        user = self.context['view'].request.user
        meeting = Meeting.objects.create(
            title=validated_data['title'],
            description=validated_data['description'],
            coordinates=validated_data['coordinates'],
            meeting_date=validated_data.get('meeting_date'),
            owner_id=user.id,
            group_type=validated_data.get('group_type', TYPE_ONE_BY_ONE),
            category=validated_data.get('category', 0)
        )
        meeting.save()

        return meeting

    class Meta:
        model = Meeting
        fields = (
            'id',
            'title',
            'meeting_date',
            'description',
            'group_type',
            'category',
            'category_obj',
            'owner',
            'coordinates',
            'subway',
            'href',
            'confirms',
            'color_status',
            'chats',
            'is_active',
        )


class MeetingCropedserializer(MeetingSerializer):
    confirms = None

    class Meta:
        model = Meeting
        fields = (
            'id',
            'title',
            'meeting_date',
            'description',
            'group_type',
            'category',
            'category_obj',
            'owner',
            'coordinates',
            'subway',
            'href',
            'color_status'
        )


class JsonResponseSerializer(serializers.Serializer):

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    status = serializers.IntegerField()
    msg = serializers.CharField(max_length=512)
    data = serializers.JSONField(required=False)


class ConfirmExtendedSerializer(ConfirmSerializer):
    meeting = MeetingCropedserializer(required=False)

    class Meta:
        model = Confirm
        fields = ('id', 'user', 'date_create', 'is_approved', 'is_rejected', 'meeting')


class AuthSerializer(serializers.Serializer):

    social_slug = serializers.CharField(max_length=16)
    external_id = serializers.IntegerField()
    token = serializers.CharField(max_length=255)
    first_name = serializers.CharField(max_length=32)
    social_avatar = serializers.URLField(max_length=1024, required=False)
    email = serializers.EmailField(required=False, max_length=32, allow_blank=True)
    from_web = serializers.BooleanField(required=False, default=False)

    def validate(self, attrs):

        social_slug = attrs.get('social_slug')
        external_id = attrs.get('external_id')
        token = attrs.get('token')
        first_name = attrs.get('first_name')
        social_avatar = attrs.get('social_avatar')
        email = attrs.get('email', '')
        from_web = attrs.get('from_web', False)

        if social_slug and external_id and token and first_name:

            # TODO IT IS NEED TO CHECK TOKEN BY VK
            # AND CHECK THAT USER WITH TOKEN HAVE VK_ID LIKE A external_id
            existing_social_data = SocialData.objects.filter(
                social_slug=social_slug,
                external_id=external_id,
            ).last()

            if existing_social_data:

                existing_social_data.token = token
                existing_social_data.save()

                user = existing_social_data.user

                user.email = email
                user.save()

                attrs['user'] = user
                return attrs

            username = hashlib.sha224('{0}{1}'.format(token, social_slug).encode('utf-8')).hexdigest()[:20]

            user = User.objects.create(
                first_name=first_name,
                username=username,
                email=email,
                from_web=from_web
            )

            user.save()

            if social_avatar and social_avatar.startswith('https://platform-lookaside.fbsbx.com'):
                name_chars = 'qwertyuiopasdfghjklzxcvbnm_-QWERTYUIOPASDFGHJKLZXCVBNM1234567890'
                name = random.shuffle(list(name_chars))[10:]
                path = '{0}/{1}/{2}.jpg'.format(settings.MEDIA_ROOT, 'user-photos', name)
                media_link = '{0}{1}/{2}.jpg'.format(settings.MEDIA_URL, 'user-photos', name)

                with open(path, 'wb+') as f:
                    f.write(requests.get(social_avatar).content)

                UserPhotos.objects.create(
                    owner=user,
                    src_original=media_link,
                    src_small=media_link,
                    src_medium=media_link,
                    is_avatar=True
                )

            social = SocialData.objects.create(
                user=user,
                social_slug=social_slug,
                external_id=external_id,
                token=token
            )

            social.save()

            attrs['user'] = user
            return attrs
        else:
            msg = 'Must include "social_slug", "external_id", "first_name" and "token".'
            raise serializers.ValidationError(msg, code='authorization')


class CountrySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        param = self.context['request'].GET.get('lang')

        if param == 'ru':
            return obj.ru_name
        else:
            return obj.name

    class Meta:
        model = Country
        fields = (
            'id',
            'name',
        )
