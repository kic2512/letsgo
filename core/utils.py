import hashlib
import random

import datetime
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from rest_framework.versioning import BaseVersioning
import logging

from core.constants import NOTIFICATION_TYPE_EMAIL

logger = logging.getLogger(__name__)


class JsonResponse(object):
    def __init__(self, status, msg, data=None):
        self.status = status
        self.msg = msg
        self.data = data


class AppVersion(BaseVersioning):
    def determine_version(self, request, *args, **kwargs):
        return request.META.get('HTTP_APP_VERSION')


def reverse_full(slug, *args, **kwargs):

    path = reverse(slug, *args, **kwargs)
    return build_absolute_url(path)


def build_absolute_url(path, anchor=None):

    if not path:
        return None

    if path.startswith('http'):
        return path

    if not anchor:
        return '{}://{}{}'.format(settings.BASE_SCHEMA, settings.BASE_DOMAIN, path)
    else:
        return '{}://{}{}#{}'.format(settings.BASE_SCHEMA, settings.BASE_DOMAIN, path, anchor)


def make_category_obj(code, category_type, name):
    return {
        'code': code,
        'type': category_type,
        'name': name
    }


def send_emails(title, body, notification_slug, to=None, is_html=False):

        if not to:
            to = ['kravcov2512@gmail.com']

        try:
            if not is_html:
                send_mail(title, body, settings.EMAIL_HOST_USER, to, fail_silently=False)
            else:
                send_mail(
                    subject=title,
                    message=body,
                    from_email=settings.EMAIL_HOST_USER,
                    recipient_list=to,
                    html_message=body,
                    fail_silently=False
                )

            from core.models import Notification, User

            for recipient in to:

                user = User.objects.filter(email=recipient).first()
                if not user:
                    continue

                Notification.objects.create(
                    user=user,
                    notification_type=NOTIFICATION_TYPE_EMAIL,
                    slug=notification_slug,
                    title=title
                )

        except Exception as e:
            logger.error(e)


def gen_notification_slug():
    salt = random.random()*10
    slug = '{0}{1}'.format(datetime.datetime.now(), salt)
    return hashlib.sha224(slug.encode('utf-8')).hexdigest()[:10]


def check_is_auth(request):

    if 'user_id' in request.session:
        return True

    return request.user.is_anonymous()
