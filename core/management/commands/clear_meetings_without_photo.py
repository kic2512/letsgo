import hashlib
import random

import datetime
import requests
from django.core.management import BaseCommand

from chat.utils import send_push
from core.models import User, Meeting
from core.utils import send_emails, gen_notification_slug


class Command(BaseCommand):
    def handle(self, *args, **options):
        meetings = Meeting.objects.filter(is_active=True)
        owners = []

        for meeting in meetings:
            owner_photo = meeting.owner.photos.filter(is_avatar=True).first()
            if not owner_photo:
                send_push(
                    users=[meeting.owner],
                    title="🤳 Let's upload your photo!",
                    message_text="People want to talk to those they see",
                    push_type='default'
                )
                owners.append(meeting.owner)

            elif owner_photo.src_original.startswith('http'):
                response = requests.get(owner_photo.src_original)

                if response.status_code != 200:
                    send_push(
                        users=[meeting.owner],
                        title="🙅 Your photo is incorrect",
                        message_text="We hide your meeting from map, please try to upload photo again",
                        push_type='default'
                    )
                    owners.append(meeting.owner)
                    meeting.is_active = False
                    meeting.save()

        send_emails(
            title='[LetsGo] Users with bad photos',
            body='User ids: {0}'.format(','.join([str(owner.id) for owner in owners])),
            notification_slug=gen_notification_slug()
        )
