import datetime
import hashlib
import json
import logging
import random

import requests
from django.core.management import BaseCommand
from django.test import Client

from django.conf import settings
from chat.models import Confirm
from core.utils import send_emails, build_absolute_url, gen_notification_slug

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Accept users requests for fake users'

    def handle(self, *args, **options):
        current_time = datetime.datetime.now()
        five_minutes_ago = current_time - datetime.timedelta(minutes=5)
        confirms = Confirm.objects.select_related('meeting', 'meeting__owner').filter(
            date_create__gt=five_minutes_ago, meeting__owner__is_fake=True
        )
        users = []
        body = ''

        for confirm in confirms:

            url = '{0}://{1}/confirm-action/{2}/'.format(
                    settings.BASE_SCHEMA,
                    settings.BASE_DOMAIN,
                    confirm.pk
            )

            response = requests.put(
                url,
                json={
                    'is_approved': True,
                    'is_read': True
                },
                headers={
                    'Authorization': 'Token {0}'.format(confirm.meeting.owner.auth_token.key),
                    'Host': '127.0.0.1'
                }
            )

            code = response.status_code

            if code == 200:
                users.append(confirm.user)
                body += '{0} was approved request from {1} (confirm_id={2}). Link to {0}: {3}\n'.format(
                    confirm.meeting.owner.first_name,
                    confirm.user.first_name,
                    confirm.id,
                    build_absolute_url('/?token={0}#chats'.format(confirm.meeting.owner.auth_token.key))
                )
            else:
                logger.error('Can not confirm request automatically. Confirm id: {0}'.format(confirm.id))
                logger.error(response.text)

        if body:
            logger.debug(body)
            send_emails(
                title='[LetsGo] Auto accept for fake users',
                body=body,
                notification_slug=gen_notification_slug(),
                to=settings.ALL_ADMINS_EMAILS
            )
