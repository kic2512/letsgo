from django.core.management import BaseCommand
import logging

from core.models import Country

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Upload countries from file to DB'

    def handle(self, *args, **options):

        with open('/tmp/countries.txt', 'r') as f:
            for line in f.readlines():
                country = line.split('|')[1]
                Country.objects.create(name=country)
