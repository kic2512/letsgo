import subprocess
from django.core.management import BaseCommand
from django.template.loader import render_to_string
from application.settings.base import BASE_DIR
import git

class Command(BaseCommand):
    help = 'Rendering deploy templates'

    def add_arguments(self, parser):
        parser.add_argument(
            '--package',
            dest='package',
            help='package version',
        )

    def handle(self, *args, **options):

        context = {
            'project_name': 'letsgo',
            'project_version': options['package']
        }
        g = git.cmd.Git('../')
        g.pull()
        print(options['package'])

        postinst_tpl = 'debian/postinst.tpl'
        control_tpl = 'debian/control.tpl'

        content = render_to_string(postinst_tpl, context)

        with open(BASE_DIR + '/../deploy_tools/main/DEBIAN/postinst', 'w') as f:
            f.write(content)

        content = render_to_string(control_tpl, context)

        with open(BASE_DIR + '/../deploy_tools/main/DEBIAN/control', 'w') as f:
            f.write(content)

        self.stdout.write('Rendering successfully completed')
        subprocess.check_call([BASE_DIR + '/../deploy_tools/build.sh', 'letsgo', '{0}'.format(options['package'])])
