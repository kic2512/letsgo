import datetime

from django.core.management import BaseCommand
from django.db.models import Q
from django.template.loader import render_to_string
from django.utils import timezone

from chat.utils import send_push
from core.models import User
import logging

from core.utils import send_emails, gen_notification_slug

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Keep users by push notifications (hourly)'

    title_template = '[LetsGo]: Send pushes to keep users, {0}'

    def hourly_users(self, current_time):
        an_hour_ago = current_time - datetime.timedelta(hours=1)
        users = User.objects.filter(date_joined__gte=an_hour_ago, confirms__isnull=True)
        send_push(
            users,
            "💪 Let's start conversation with someone!",
            'Open the map and click on the person you want to meeting.',
            push_type='default'
        )

        send_emails(
            title=self.title_template.format('hourly'),
            body='Users ids: {0}'.format(', '.join([str(user.id) for user in users])),
            notification_slug=gen_notification_slug()
        )

    def three_hourly_users(self, current_time):
        three_hours_ago = current_time - datetime.timedelta(hours=3)
        two_hours_ago = current_time - datetime.timedelta(hours=2)
        users = User.objects.filter(
            date_joined__gte=three_hours_ago,
            date_joined__lte=two_hours_ago,
            created_meetings__isnull=True
        )

        if not users:
            return

        send_push(
            users,
            "👫 Create a meeting",
            'Indicate on the map what place you want to visit and find a partner.',
            push_type='default'
        )

        send_emails(
            title=self.title_template.format('three hourly'),
            body='Users ids: {0}'.format(', '.join([str(user.id) for user in users])),
            notification_slug=gen_notification_slug()
        )

    def six_hourly_users(self, current_time):
        six_hours_ago = current_time - datetime.timedelta(hours=6)
        five_hours_ago = current_time - datetime.timedelta(hours=5)
        users = User.objects.filter(date_joined__gte=six_hours_ago, date_joined__lte=five_hours_ago)

        if not users:
            return

        send_push(
            users,
            "🎿 Do you want to go somewhere?",
            'Find interesting meeting in the map view.',
            push_type='default'
        )

        send_emails(
            title=self.title_template.format('six hours'),
            body='Users ids: {0}'.format(', '.join([str(user.id) for user in users])),
            notification_slug=gen_notification_slug()
        )

    def users_without_image(self, current_time):
        three_days_ago = current_time - datetime.timedelta(days=3)

        users_1 = (
            User.
            objects.
            filter(photos__isnull=True).
            filter(notifications__isnull=True)
        )

        users_2 = User.objects.filter(photos__isnull=True).exclude(notifications__date_create__gte=three_days_ago)

        users = (users_1 | users_2).distinct('id')

        if not users:
            return

        send_push(
            users,
            "❗Let's upload your photos!",
            'Upload more photos and get the more contacts!',
            push_type='default'
        )

        for user in users:
            if not user.email:
                continue

            n_slug = gen_notification_slug()

            send_emails(
                title="❗Hi friend, upload more photos and get the more contacts!",
                body=render_to_string(
                    'letters/without_photo_users.html',
                    context={
                        'notification_slug': n_slug,
                        'user_id': user.id
                    }),
                to=[user.email] if not self.is_test else None,
                is_html=True,
                notification_slug=n_slug
            )

        send_emails(
            title=self.title_template.format('without avatar'),
            body='Users ids: {0}'.format(', '.join([str(user.id) for user in users])),
            notification_slug=gen_notification_slug()
        )

    def users_without_photo_and_have_meeting(self, current_time):
        users = User.objects.filter(
            photos__isnull=True,
            created_meetings__isnull=False,
            created_meetings__is_active=True
        )

        if not users:
            return

        send_push(
            users,
            "❗Let's upload your photos!",
            'Add your photos and people will start to click on your picture.',
            push_type='default'
        )

        send_emails(
            title=self.title_template.format('without avatar and have created meetings'),
            body='Users ids: {0}'.format(', '.join([str(user.id) for user in users])),
            notification_slug=gen_notification_slug()
        )

    def add_arguments(self, parser):
        parser.add_argument(
            '--test',
            type=int,
            dest='test',
            default=0,
            help='send to daddy',
        )

    def handle(self, *args, **options):

        self.is_test = True if options.get('test') > 0 else False

        current_time = timezone.now()

        self.hourly_users(current_time)
        self.three_hourly_users(current_time)
        self.six_hourly_users(current_time)
        self.users_without_image(current_time)
        self.users_without_photo_and_have_meeting(current_time)
