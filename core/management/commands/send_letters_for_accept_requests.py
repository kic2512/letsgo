import logging

import datetime
from django.core.management import BaseCommand
from django.db.models import Q
from django.template.loader import render_to_string

from chat.models import Confirm
from core.utils import send_emails, build_absolute_url, gen_notification_slug

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send email for users who have was accepted for chat'

    def add_arguments(self, parser):
        parser.add_argument(
            '--test',
            type=int,
            dest='test',
            default=0,
            help='send to daddy',
        )

    def handle(self, *args, **options):

        self.is_test = True if options['test'] > 0 else False

        current_time = datetime.datetime.now()
        fifteen_minutes_ago = current_time - datetime.timedelta(minutes=15)

        requests = (
            Confirm.
            objects.
            select_related('meeting__owner').
            filter(
                is_approved=True,
                is_rejected=False,
                user__last_login__lte=fifteen_minutes_ago,
                user__messages__isnull=True,
                last_modify__gte=fifteen_minutes_ago
            ).
            order_by('id')
        )

        objects = {}

        for request in requests:

            owner = request.meeting.owner
            you = request.user
            chat = request.meeting.chats.filter(owner=owner, users__in=[you], meeting_id=request.meeting_id).first()

            if not you.email or not chat:
                continue

            if hasattr(owner.get_avatar(), 'src_original'):
                photo = build_absolute_url(owner.get_avatar().src_original)
            else:
                photo = build_absolute_url(owner.get_avatar()['src_medium'])

            if owner.id not in objects:
                objects[owner.id] = {
                    'user_id': you.id,
                    'chat_id': chat.id,
                    'first_name': you.first_name,
                    'email': you.email,
                    'unread_count': 1,
                    'from': {owner.first_name},
                    'users': [{
                        'sender_name': owner.first_name,
                        'avatar': photo,
                        'date': request.date_create,
                    }]
                }
            else:
                objects[owner.id]['unread_count'] += 1
                objects[owner.id]['from'].add(owner.first_name)
                objects[owner.id]['users'].append({
                    'sender_name': owner.first_name,
                    'avatar': photo,
                    'date': request.date_create,
                })

        for key, obj in objects.items():

            title = '👍' + ' You was approved by {0}!'.format(','.join([str(x) for x in obj['from']]))

            n_slug = gen_notification_slug()

            body = render_to_string(
                'letters/accepted_requests.html',
                context={
                    'notification_slug': n_slug,
                    'user_id': obj['user_id'],
                    'chat_id': obj['chat_id'],
                    'first_name': ','.join([str(x) for x in obj['from']]),
                    'users': obj['users'],
                    'unread_count': obj['unread_count']
                }
            )

            send_emails(
                title=title,
                body=body,
                to=[obj['email']] if not self.is_test else None,
                is_html=True,
                notification_slug=n_slug
            )
