import logging

import datetime
from django.core.management import BaseCommand
from django.db.models import Q
from django.template.loader import render_to_string

from core.constants import MALE
from core.models import User
from core.utils import send_emails, gen_notification_slug

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send email for users who have visit early'

    def add_arguments(self, parser):
        parser.add_argument(
            '--test',
            type=int,
            dest='test',
            default=0,
            help='send to daddy',
        )

    def handle(self, *args, **options):

        self.is_test = True if options['test'] > 0 else False

        current_time = datetime.datetime.now()
        one_day_ago = current_time - datetime.timedelta(days=2)
        three_days_ago = current_time - datetime.timedelta(days=3)
        five_days_ago = current_time - datetime.timedelta(days=5)

        one_month_ago = current_time - datetime.timedelta(weeks=4)
        two_month_ago = current_time - datetime.timedelta(weeks=8)
        three_month_ago = current_time - datetime.timedelta(weeks=12)
        one_year_ago = current_time - datetime.timedelta(days=365)

        intervals = [
            (one_day_ago, three_days_ago),
            (three_days_ago, five_days_ago),
            (five_days_ago, one_month_ago),
            (one_month_ago, two_month_ago),
            (two_month_ago, three_month_ago),
            (three_month_ago, one_year_ago),
        ]

        for first_interval, second_interval in intervals:
            users = User.objects.filter(
                last_login__lt=first_interval,
                last_login__gte=second_interval,
                email__isnull=False,
            ).filter(
                Q(notifications__date_create__lt=first_interval) | Q(notifications__isnull=True)
            )

            for user in users:

                n_slug = gen_notification_slug()

                if first_interval <= five_days_ago:
                    title = '{0}'.format(
                        '🙋' if user.gender == MALE else '🙋♂️') + 'Hi bro! We have a new users. Check it out!'
                    body = render_to_string(
                        'letters/not_visit_users.html',
                        context={
                            'notification_slug': n_slug,
                            'user_id': user.id,
                            'first_name': user.first_name,
                        }
                    )
                else:
                    title = '{0}'.format(
                        '🙋' if user.gender == MALE else '🙋♂️') + "Let's join to us❗"
                    body = render_to_string(
                        'letters/not_visit_users_long_time.html',
                        context={
                            'notification_slug': n_slug,
                            'user_id': user.id,
                            'first_name': user.first_name,
                        }
                    )

                send_emails(
                    title=title,
                    body=body,
                    to=[user.email] if not self.is_test else None,
                    is_html=True,
                    notification_slug=n_slug
                )
