from django.core.management import BaseCommand
from core.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = User.objects.all()
        for user in users:
            meetings = user.created_meetings.all().order_by('date_create')
            if meetings:
                meetings.update(is_active=False)
                last = meetings.last()
                last.is_active = True
                last.save()
