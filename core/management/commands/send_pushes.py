from django.core.management import BaseCommand

from chat.utils import send_push
from core.models import User


class Command(BaseCommand):
    help = 'Send push to users'

    def add_arguments(self, parser):
        parser.add_argument(
            '--title',
            dest='title',
            help='push title',
        )
        parser.add_argument(
            '--text',
            dest='text',
            help='push text',
        )

        parser.add_argument(
            '--ids',
            dest='ids',
            help='push text',
        )

    def handle(self, *args, **options):

        title = options['title']
        msg = options['text']
        ids = options.get('ids')

        if not title:
            return

        if not ids:
            users = User.objects.all()
        else:
            users = User.objects.filter(id__in=[int(x) for x in ids.split(',')])

        for u in users:
            print(u.first_name)

        send_push(users, title, msg, push_type='default')
