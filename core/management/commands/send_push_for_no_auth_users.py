import logging
import random

from django.core.management import BaseCommand

from chat.utils import send_push
from core.models import UnAuthorizedUsersKeys

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send email for users who have unread messages'

    def handle(self, *args, **options):

        tokens = UnAuthorizedUsersKeys.objects.all()

        possible_titles = [
            "Hi bro, what's up?",
            "Hi! Let's join to us!",
            "Do you know how say hello in Russian? Just ask!",
            "New people in Moscow, say hello to them!"
        ]

        possible_messages = [
            "Open the map and click on the person you want to meeting.",
            "Open the map and click on the person you want to talk.",
            "Indicate on the map what place you want to visit and find a partner.",
            "Let's find interesting meeting in the map view!",
            "Add your photos start conversation with someone!."
        ]

        random.shuffle(possible_titles)
        random.shuffle(possible_messages)

        send_push(
            users=None,
            title=possible_titles[0],
            message_text=possible_messages[0],
            push_type='default',
            tokens=tokens
        )
