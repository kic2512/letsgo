import logging

from django.core.management import BaseCommand

from core.models import Country

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'add translate to countries'

    def handle(self, *args, **options):

        with open('/tmp/_countries.csv', 'r') as f:

            for line in f.readlines()[1:]:
                line = line.replace('"', '')
                name = line.split(';')[4]
                country = Country.objects.filter(name=name).first()
                if country:
                    country.ru_name = line.split(';')[1]
                    country.save()
