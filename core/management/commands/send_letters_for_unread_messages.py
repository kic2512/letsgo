import logging

import datetime
from django.core.management import BaseCommand
from django.db.models import Count
from django.template.loader import render_to_string

from chat.models import Message, MessageReadState
from core.utils import send_emails, build_absolute_url, gen_notification_slug

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send email for users who have unread messages'

    def generate_objects_by_readstates(self, read_states):

        objects = {}

        for state in read_states:

            if not state.user.email:
                continue

            if state.chat.owner_id == state.user_id:
                sender = state.chat.users.first()
            else:
                sender = state.chat.owner

            if objects.get(state.user_id):
                message = state.chat.messages.exclude(id__in=objects[state.user_id]['read_messages_ids']).order_by(
                    'id').last()
            else:
                message = state.chat.messages.order_by('id').last()

            if hasattr(sender.get_avatar(), 'src_original'):
                photo = build_absolute_url(sender.get_avatar().src_original)
            else:
                photo = build_absolute_url(sender.get_avatar()['src_medium'])

            if state.user_id not in objects:
                objects[state.user_id] = {
                    'id': state.user_id,
                    'first_name': state.user.first_name,
                    'email': state.user.email,
                    'unread_count': 1,
                    'from': {message.author.first_name},
                    'read_messages_ids': [message.id],
                    'users': [{
                        'sender_name': sender.first_name,
                        'avatar': photo,
                        'date': message.date_create,
                        'text': message.text,
                        'chat_id': message.chat_id,
                    }]
                }
            else:
                objects[state.user_id]['unread_count'] += 1
                objects[state.user_id]['from'].add(sender.first_name)
                objects[state.user_id]['read_messages_ids'].append([message.id])
                objects[state.user_id]['users'].append({
                    'sender_name': message.author.first_name,
                    'avatar': photo,
                    'date': message.date_create,
                    'text': message.text,
                    'chat_id': message.chat_id,
                })

        return objects

    def generate_objects_by_messages(self, messages):
        objects = {}

        for message in messages:

            if message.author_id == message.chat.owner_id:
                user = message.chat.users.first()
            else:
                user = message.chat.owner

            if not user.email:
                continue

            if hasattr(message.author.get_avatar(), 'src_original'):
                photo = build_absolute_url(message.author.get_avatar().src_original)
            else:
                photo = build_absolute_url(message.author.get_avatar()['src_medium'])

            if user.id not in objects:
                objects[user.id] = {
                    'id': user.id,
                    'first_name': user.first_name,
                    'email': user.email,
                    'unread_count': 1,
                    'from': {message.author.first_name},
                    'read_messages_ids': [message.id],
                    'users': [{
                        'sender_name': message.author.first_name,
                        'avatar': photo,
                        'date': message.date_create,
                        'text': message.text,
                        'chat_id': message.chat_id,
                    }]
                }
            else:
                objects[user.id]['unread_count'] += 1
                objects[user.id]['from'].add(message.author.first_name)
                objects[user.id]['read_messages_ids'].append([message.id]),
                objects[user.id]['users'].append({
                    'sender_name': message.author.first_name,
                    'avatar': photo,
                    'date': message.date_create,
                    'text': message.text,
                    'chat_id': message.chat_id,
                })

        return objects

    def add_arguments(self, parser):
        parser.add_argument(
            '--test',
            type=int,
            dest='test',
            default=0,
            help='send to daddy',
        )

    def handle(self, *args, **options):

        self.is_test = True if options['test'] > 0 else False

        current_time = datetime.datetime.now()
        fifteen_minutes_ago = current_time - datetime.timedelta(minutes=15)

        chats_ids = Message.objects.filter(date_create__gt=fifteen_minutes_ago).values_list('chat_id', flat=True).distinct()
        read_states = (
            MessageReadState.
            objects.
            select_related('user', 'chat').
            filter(chat_id__in=chats_ids, last_read__lt=fifteen_minutes_ago).
            order_by('last_read')
        )

        messages = (
            Message.
            objects.
            select_related('author').
            filter(chat_id__in=chats_ids).
            filter(date_create__gt=fifteen_minutes_ago).
            filter(chat__read_states__isnull=False).
            annotate(states_cnt=Count('chat_id')).
            filter(states_cnt=1).
            order_by('id')
        )

        objects = self.generate_objects_by_readstates(read_states)
        objects.update(self.generate_objects_by_messages(messages))

        if not objects.items():
            logger.warning('Not users with unread messages')
            return None

        for key, obj in objects.items():
            if obj['unread_count'] < 2:
                title = '✉️ You have new message from {0}!'.format(','.join([str(x) for x in obj['from']]))
            else:
                title = '✉️ You have new messages from {0}!'.format(','.join([str(x) for x in obj['from']]))

            notification_slug = gen_notification_slug()

            body = render_to_string(
                'letters/not_read_message.html',
                context={
                    'notification_slug': notification_slug,
                    'user_id': obj['id'],
                    'first_name': ','.join([str(x) for x in obj['from']]),
                    'users': obj['users'],
                    'unread_count': obj['unread_count']
                }
            )

            send_emails(
                title=title,
                body=body,
                to=[obj['email']] if not self.is_test else None,
                is_html=True,
                notification_slug=notification_slug
            )
