import logging

import datetime
from django.core.management import BaseCommand
from django.template.loader import render_to_string

from chat.models import Confirm
from core.utils import send_emails, build_absolute_url, gen_notification_slug

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send email for users who have unread requests'

    def add_arguments(self, parser):
        parser.add_argument(
            '--test',
            type='int',
            dest='test',
            default=0,
            help='send to daddy',
        )

    def handle(self, *args, **options):

        self.is_test = True if options['test'] > 0 else False

        current_time = datetime.datetime.now()
        fifteen_minutes_ago = current_time - datetime.timedelta(minutes=15)

        requests = (
            Confirm.
            objects.
            select_related('meeting__owner').
            filter(is_approved=False, is_rejected=False, date_create__gt=fifteen_minutes_ago).
            order_by('id')
        )

        objects = {}

        for request in requests:

            receiver = request.meeting.owner
            sender = request.user

            if not receiver.email:
                continue

            if hasattr(sender.get_avatar(), 'src_original'):
                photo = build_absolute_url(sender.get_avatar().src_original)
            else:
                photo = build_absolute_url(sender.get_avatar()['src_medium'])

            if sender.id not in objects:
                objects[sender.id] = {
                    'user_id': receiver.id,
                    'first_name': receiver.first_name,
                    'email': receiver.email,
                    'unread_count': 1,
                    'from': {sender.first_name},
                    'users': [{
                        'sender_name': sender.first_name,
                        'avatar': photo,
                        'date': request.date_create,
                    }]
                }
            else:
                objects[sender.id]['unread_count'] += 1
                objects[sender.id]['from'].add(sender.first_name)
                objects[sender.id]['users'].append({
                    'sender_name': sender.first_name,
                    'avatar': photo,
                    'date': request.date_create,
                })

        for key, obj in objects.items():

            if obj['unread_count'] < 2:
                title = '⁉️' + ' You have new chat request from {0}!'.format(','.join([str(x) for x in obj['from']]))
            else:
                title = '⁉️' + ' You have new chat requests from {0}!'.format(','.join([str(x) for x in obj['from']]))

            n_slug = gen_notification_slug()

            body = render_to_string(
                'letters/not_read_request.html',
                context={
                    'notification_slug': n_slug,
                    'user_id': obj['user_id'],
                    'first_name': ','.join([str(x) for x in obj['from']]),
                    'users': obj['users'],
                    'unread_count': obj['unread_count']
                }
            )

            send_emails(
                title=title,
                body=body,
                to=[obj['email']] if not self.is_test else None,
                is_html=True,
                notification_slug=n_slug
            )
