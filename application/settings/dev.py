from application.settings.base import *  # NOQA

DEBUG = True
TEMPLATE_DEBUG = True

BASE_DOMAIN = '127.0.0.1:8000'
BASE_SCHEMA = 'http'

TORNADO_PORT = 9001

print('use dev settings')
