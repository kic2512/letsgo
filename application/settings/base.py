"""
Django settings for together project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

import logging

from application.settings.secret import *
import redis


# TODO MOVE TO CONFIG
REDIS_DB = redis.Redis(db=1, decode_responses=True)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# CONFIG_PATH = os.environ['TOGETHER_CONF']
#
# config = configparser.ConfigParser()
# config.read(CONFIG_PATH, encoding='utf-8')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['www.russian-friends.com', 'russian-friends.com', '80.87.201.72', '127.0.0.1', 'localhost']

BASE_DOMAIN = 'russian-friends.com'
BASE_SCHEMA = 'https'

AUTH_USER_MODEL = 'core.user'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.postgres',
    'django.contrib.staticfiles',
    'django.contrib.gis',

    'rest_framework',
    'rest_framework.authtoken',
    'core',
    'location',
    'tags',
    'chat',
    'payments',
    'django_js_error_hook',
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
}

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

SOCIAL_AUTH_LOGIN_URL = 'oauth-login/'

ROOT_URLCONF = 'application.urls'

WSGI_APPLICATION = 'application.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'OPTIONS': {
            'options': '-c search_path=public'
        },
    }
}

ADMINS = [('ILIA', 'kravcov2512@gmail.com')]

ALL_ADMINS_EMAILS = ['xyets777@gmail.com', 'kravcov2512@gmail.com', 'kyupetrov@gmail.com']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/uwsgi-letsgo.log',
        },
        'tornado': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/tornado-letsgo.log',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['file'],
            'level': 'ERROR',
            'propagate': True,
        },
        'management_commands': {
            'handlers': ['tornado'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'javascript_error': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
    },
}

JAVASCRIPT_ERROR_ID = 'django.request'
JAVASCRIPT_ERROR_CSRF_EXEMPT = True

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "../static_root")

MEDIA_URL = '/uploads/'
MEDIA_ROOT = os.path.join(BASE_DIR, "../uploads")

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "../static"),
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, '/../tpl/'),
            os.path.join(BASE_DIR, '/../deploy_tools/tpl/')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

            ],
        },
    }
]

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'APP_DIRS': True,
    'DIRS': [BASE_DIR + '/../tpl/', BASE_DIR + '/../deploy_tools/tpl/'],
    'OPTIONS': {
        'context_processors': ['django.contrib.auth.context_processors.auth']
    }
}]

TORNADO_PORT = 9000

