define([
    'backbone',
    'underscore',
    'collections/meetings',
    'models/map',
    'models/user',
    'router',
    'toastr'
], function (Backbone, _, meetingsCollection, MapModel, UserModel, router, toastr) {

    var View = Backbone.View.extend({
        el: $('#map'),
        user: new UserModel(),
        events: {
            'click .customMarker': 'showMeeting'
        },

        initialize: function () {
            this.listenTo($('#map'), 'clickPhoto', this.showMeeting);
            this.show();
            return this;
        },

        render: function () {
            if (window.isInit === true) {
                this.collection.fetch({
                    success: function () {
                        var meetings = this.collection.toJSON();
                        window.setMarkers(meetings);
                    }.bind(this),

                    error: function () {
                        this.showWarning("Something went wrong :( Please, try repeat later...");
                    }.bind(this)
                });
            }
            return this;
        },

        show: function () {
            this.$el.show();
            this.render();
            this.el.scrollIntoView();
            return this;
        },

        showCreateMeeting: function () {

            console.log('show create meeting');
            var tokenHeader = window.getTokenHeader();

            if (!tokenHeader) {
                return false;
            }

            this.hideCreateMeetingElements();

            var div = document.createElement('div');
            var p = document.createElement('p');
            var button = document.createElement('button');

            div.setAttribute('id', 'map-title');
            button.setAttribute('id', 'js-meeting-create');


            p.innerHTML = "Where do you want to meet?";

            button.innerHTML = "Create";
            button.style.margin = "25px";
            button.style.marginBottom = "45px";
            button.style.padding = "10px";
            button.style.fontSize = "28px";
            button.style.color = "#212121";
            button.style.backgroundColor = '#8BC34A';
            button.style.paddingLeft = '35px';
            button.style.paddingRight = '35px';
            button.style.border = '1px solid #212121';

            var xhr = new XMLHttpRequest();

            button.onclick = function () {
                var lat = window.userMarker.getPosition().lat();
                var lng = window.userMarker.getPosition().lng();

                this.user.fetch().done(function () {

                    var userData = {
                        'title': this.user.attributes.first_name,
                        'description': this.user.attributes.about,
                        'group_type': 0,
                        'coordinates': {
                            'lat': lat,
                            'lng': lng
                        }
                    };
                    xhr.open('POST', '/meetings-list/', true);
                    xhr.setRequestHeader('Authorization', tokenHeader);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send(JSON.stringify(userData));

                }.bind(this));

                xhr.onloadend = function (response) {
                    document.getElementById('map-title').remove();
                    document.getElementById('js-meeting-create').remove();
                    window.userMarker.setMap(null);

                    this.render();

                    var currentMeeting = JSON.parse(response.currentTarget.responseText);

                    if(parseInt(currentMeeting.id)) {
                        Backbone.history.navigate('meeting/' + currentMeeting.id, true);

                        toastr['success']('Your event was successfully created');
                    }
                    else {
                        console.log(console.trace());
                        toastr['error']('Something went wrong :( Please try again later...');
                    }

                }.bind(this);

                xhr.onerror = function () {
                    toastr['error']('Something go wrong :( Please try later');

                    document.getElementById('map-title').remove();
                    document.getElementById('js-meeting-create').remove();
                    window.userMarker.setMap(null);
                };


            }.bind(this);

            div.appendChild(p);
            div.style.fontSize = "32px";
            div.style.color = '#212121';
            div.style.padding = '20px';
            div.style.backgroundColor = "rgba(167, 167, 167, 0.7)";
            div.style.border = '#4a4646';
            div.style.borderWidth = '1px';
            div.style.borderStyle = 'solid';
            div.style.marginTop = '20px';

            window.map.controls[google.maps.ControlPosition.TOP].push(div);
            window.map.controls[google.maps.ControlPosition.BOTTOM].push(button);

            var mapCenter = window.map.getCenter();

            this.clearMarkers();


            var userMarker = new google.maps.Marker({
                position: new google.maps.LatLng(mapCenter.lat(), mapCenter.lng()),
                draggable: true
            });

            userMarker.setMap(window.map);

            window.userMarker = userMarker;

            return this;
        },

        clearMarkers: function (a) {
            _.each(window.markersList, function(m){m.setMap(null)});
        },

        hideCreateMeetingElements: function () {

            if(document.getElementById('map-title')){
                document.getElementById('map-title').remove();
            }

            if(document.getElementById('js-meeting-create')){
                document.getElementById('js-meeting-create').remove();
            }

            if(window.userMarker){
                window.userMarker.setMap(null);
            }

            return this;
        },

        hide: function () {
            this.hideCreateMeetingElements();
            return this;
        },

        showMeeting: function (obj) {
            //this.trigger('clickPhoto', this, obj);
            var meetingId = parseInt(obj.currentTarget.dataset.id);
            if(meetingId){
                Backbone.history.navigate('meeting/' + meetingId, true);
            }
            else {
                toastr['error']('Something went wrong :( Please try again later');
            }
        },

        showSuccess: function (message) {
            toastr['success'](message);
        },

        showWarning: function (message) {
            console.log(console.trace());
            toastr['error'](message);
        }
    });

    var map = new MapModel();

    return new View({
        model: map,
        collection: meetingsCollection
    });
});