define([
    'backbone',
    'underscore',
    'models/user',
    'toastr'
], function (Backbone, _, UserModel, toastr) {

    var View = Backbone.View.extend({
        el: $('#right-column'),
        template: _.template($('#user-settings-tml').html()),
        templateNoAuth: _.template($('#no-auth-tml').html()),

        events: {
            'click .js-user-settings': 'show',
            'click .js-user-save': 'saveProfile',
            'change #fileInput': 'uploadPhoto',
            'onchange #fileInput': 'uploadPhoto'
        },

        initialize: function () {
            if (window.userToken) {
                this.render();
            }
            else {
                this.showNoAuth();
            }
            return this;
        },

        showNoAuth: function () {
            this.$el.html(this.templateNoAuth());
            this.$el.show();
        },

        render: function () {
            this.$el.html(_.template($('#preloader-tml').html()));
            this.model.fetch({
                success: function (model) {
                    this.$el.html(this.template({
                        user: model.toJSON()
                    }));
                }.bind(this),

                error: function () {
                    console.log(console.trace());
                    toastr['error']('Something go wrong :( Please try later')
                }.bind(this)
            });
            return this;
        },

        show: function () {
            if (window.userToken){
                this.render();
                this.$el.show();
                this.el.scrollIntoView();
                return this;
            }
            else {
                window.getTokenHeader();
                toastr['warning']('You need to register');
            }
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        showSuccess: function (message) {
            toastr['success'](message);
        },

        showWarning: function (message) {
            console.log(console.trace());
            toastr['warning'](message);
        },

        saveProfile: function () {

            this.model.set({
                first_name: $('#first_name').val(),
                about: $('#about').val(),
                birth_date: $('#birth-year option:selected').val() + '-' + $('#birth-month option:selected').val() + '-' + $('#birth-day option:selected').val(),
                gender: $("input:radio:checked").val()
            });

            this.model.save(null, {
                success: function (model, response) {
                    if ('status' in response && response.status == 'OK') {
                        this.render();

                        toastr['success']('Your data successfully updated!');

                        this.trigger('to-poll');
                    }
                    else if ('error' in response) {
                        this.showWarning(response.error);
                    }
                }.bind(this),

                error: function (model, response) {
                    this.showWarning('Error, please write report to baumansofware@gmail.com');
                }.bind(this)
            });
        },

        uploadPhoto: function () {

            var uploadPrefixURL = this.model.uploadPhotoUrl;
            var tokenHeader = this.model.getTokenHeader();
            var context = this;

            if (window.File && window.FileReader && window.FileList && window.Blob) {

                var files = document.getElementById('fileInput').files;

                for(var i = 0; i < files.length; i++) {
                    var currentFile = files[i];
                    var currentFileName = files[i].name;

                    var xhr = new XMLHttpRequest();

                    xhr.upload.onloadend = function (p1) {
                        console.log('Uploaded');
                        context.render();
                    };

                    xhr.upload.onerror = function (p1) {
                        console.log('Uploaded error');
                    };

                    var uploadUrl = uploadPrefixURL + currentFileName;

                    xhr.open('PUT', uploadUrl, true);
                    xhr.setRequestHeader('Authorization', tokenHeader);
                    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded; charset=utf-8");
                    xhr.send(currentFile);

                }
            } else {
                toastr['error']('The File APIs are not fully supported in this browser.');
            }
        }
    });
    var userModel = new UserModel();
    return new View({model: userModel});
});