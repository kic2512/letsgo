/**
 * Created by ikravtsov on 02/06/2018.
 */
define([
    'backbone',
    'underscore',
    'collections/requests',
    'toastr'
], function (Backbone, _, requestsCollection, toastr) {

    var View = Backbone.View.extend({
        el: $('#right-column'),
        template: _.template($('#requests-tml').html()),

        events: {
            'click .js-accept-request': 'acceptRequest',
            'click .js-reject-request': 'rejectRequest'
        },

        initialize: function () {
            return this;
        },

        render: function () {
            this.collection.fetch({
                success: function (response) {
                    console.log('fetch requests');
                    console.log(response.models[0]);
                    this.$el.html(this.template({
                        requests: response.models
                    }));
                }.bind(this),

                error: function () {
                }.bind(this)
            });
            return this;
        },

        show: function () {

            var unredReqSpan = document.getElementById('unred-requests');
            unredReqSpan.innerHTML = 0;
            unredReqSpan.style.display = 'none';

            this.render();
            this.$el.show();
            this.el.scrollIntoView();
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        acceptRequest: function (e) {
            var requestId = e.target.dataset.requestid;
            this.sendAction(requestId, {"is_approved": true});
            this.render();
            Backbone.history.navigate('chats');
            toastr['info']('Now you have a new chat!')
        },

        rejectRequest: function (e) {
            var requestId = e.target.dataset.requestid;
            this.sendAction(requestId, {"is_rejected": true});
            this.render();
        },

        sendAction: function (requestId, data) {

            var tokenHeader = window.getTokenHeader();

            var xhr = new XMLHttpRequest();

            xhr.upload.onloadend = function (p1) {
                console.log('action was sent');
                this.trigger("rerender", this);
            }.bind(this);

            xhr.upload.onerror = function (p1) {
                this.showWarning("Something went wrong :( Please try again later...");
            };

            xhr.open('PUT', this.collection.urlBaseAction + requestId + '/', true);
            xhr.setRequestHeader('Authorization', tokenHeader);
            xhr.setRequestHeader("Content-type","application/json");
            xhr.send(JSON.stringify(data));
        },

        showSuccess: function (message) {
            this.showSnackbar(message);
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        }

    });
    return new View({collection: requestsCollection});
});
