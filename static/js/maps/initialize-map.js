function initMap() {
    // Create the map.

    //noinspection JSUnresolvedVariable
    //google.load('visualization', '1', {'packages':['corechart']});

    map = new google.maps.Map(document.getElementById('regions_div'), {
        zoom: 3,
        maxZoom: 7,
        minZoom: 2,
        center: {lat: 45.0, lng: 35.0},
        mapTypeId: google.maps.MapTypeId.TERRAIN
    });

    var customMapType = new google.maps.StyledMapType(
        [{
            stylers: [
                {hue: 'white'},
                {gamma: 0.5},
                {weight: 0.5}
            ]
        }, {
            elementType: 'labels',
            stylers: [{visibility: 'on'}]  // добавили подписи к странам
        }, {
            featureType: 'road',
            elementType: 'geomerty',
            stylers: [{visibility: 'off'}] // убрали дорги
        }, {
            featureType: 'poi.park',
            elementType: 'geomerty',
            stylers: [{visibility: 'off'}] // убрали парки
        }, {
            featureType: 'water',
            stylers: [{color: '#575757'}]  // вода - серая
        }, {
            featureType: 'landscape',
            stylers: [{color: '#BAC'}]
        }
        ], {
            name: 'Custom Style'
        });

    var customMapTypeId = 'custom_style';

    map.mapMarkers = [];
    map.cityMarkers = [];

    map.mapTypes.set(customMapTypeId, customMapType);
    map.setMapTypeId(customMapTypeId);

    map.isCountryView = true;
    map.isCityView = false;

    map.addListener('zoom_changed', function () {
        var zoom = this.getZoom();
        var zoomCrossOver = 3;

        if (zoom > zoomCrossOver && map.isCountryView === true) {
            map.isCountryView = false;
            map.isCityView = true;

            drawCircles(window.cityData);
        }
        else if (zoom <= zoomCrossOver && map.isCityView === true) {
            map.isCountryView = true;
            map.isCityView = false;

            drawCircles(window.countryData);
        }
    });
}

function drawCircles(data, questionTitle) {
    clearMarkers();

    var opt = null;
    var colors = window.MARKER_COLORS;

    for (var location in data) {
        if (data.hasOwnProperty(location)) {

            var total = data[location].total;

            if (typeof total === 'undefined') {
                continue;
            }

            var lat = parseFloat(data[location]["lat"]);
            var lng = parseFloat(data[location]["lng"]);
            var dataTable = makeTableData(data[location]['statistics']);

            if (opt == null) {
                opt = {};
                opt["legend"] = {};
                for (var k = 1; k < dataTable.table.length; k++) {
                    var title = dataTable.table[k][0];
                    opt["legend"][title] = colors[k - 1];
                }
            }

            var colorClass = colors[dataTable.maxIndex % colors.length];

            var numb = Math.round(Math.random() * 10);
            var durationLabel = " label-" + numb;

            var marker = new MarkerWithLabel({
                map: map,
                position: {lat: lat, lng: lng},
                clickable: true,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 0
                },
                labelAnchor: new google.maps.Point(10, 10),
                labelClass: colorClass + " label-0" + durationLabel // the CSS class for the label
            });

            marker.table = dataTable.table;
            marker.questionTitle = questionTitle + ' (' + data[location]["name"] + ')';
            marker.location = data[location]["name"];

            var clickHandler = function () {
                window.globalView.markerClick(this.table, this.location);
            };
            //google.maps.event.addListener(marker, "click", clickHandler);
            google.maps.event.addListener(marker, "mousedown", clickHandler);

            map.mapMarkers.push(marker);
        }
    }
}

function getRadius(numb) {
    return 100000;
}

function makeTableData(variants) {
    var table = [];
    var maxAmount = -1, maxIndex = -1, I = 0;
    var row;
    for (var variant in variants) {
        if (variants.hasOwnProperty(variant)) {
            row = [variant, variants[variant]];
            table.push(row);

            if (maxAmount < variants[variant]) {
                maxAmount = variants[variant];
                maxIndex = I;
            }
            I++;
        }
    }
    return {
        table: table,
        max: maxAmount,
        maxIndex: maxIndex
    };
}

function clearMarkers() {
    if (map.mapMarkers) {
        for (var i = 0; i < map.mapMarkers.length; i++) {
            map.mapMarkers[i].setMap(null);
        }
    }
    map.mapMarkers = [];
}

//-------------------------------------------------------------------

function drawChart(marker, colors) {
    if (window.prevChart != null) {
        window.prevChart.close();
    }

    // Create the data table.
    var data = new google.visualization.arrayToDataTable(marker.table);

    // Set chart options
    var options = {
        title: marker.questionTitle,
        is3D: true,
        colors: colors
    };

    var node = document.getElementById('chart_div');
    $(node).addClass('info-window-element');
    $(node).addClass('chart-to-image');

    var chart = new google.visualization.PieChart(node);
    chart.draw(data, options);

    var share = document.createElement('div');
    $(share).addClass('info-window-element');

    var infoWindow = new google.maps.InfoWindow();

    sendChartPicture(chart.getImageURI())
        .done(function () {
            var renderedBtn = prepareSharing();
            $(share).html(renderedBtn);

            infoWindow.setContent('<div class="info-window-element chart-to-image">' +
            $(node).html() + '</div><div class="info-window-element">' + $(share).html() + "</div>");

            $.when(infoWindow.open(marker.getMap(), marker))
                .done(function () {
                    window.prevChart = infoWindow;
                    setShare();
                });
        });
}

function sendChartPicture(imageUri) {
    var r = $.Deferred();

    var parts = imageUri.split(',');
    if (parts.length === 2) {
        var dataParts = parts[0].split(':');
        var dataType = dataParts.length === 2 ? dataParts[1] : dataParts;
        var data = parts[1];
        $.post('/upload-share-img/', {dataType: dataType, data: data})
            .done(function (response) {
                if (response.status === 'OK' && response.data) {
                    $("meta[property='og\\:image']").attr("content", response.data.img_url);
                    r.resolve();
                }
                else if (response.status === 'ERROR') {
                    console.log(response.error);
                    r.resolve();
                }
            })
            .fail(function (xhr, textStatus) {
                console.log(textStatus);
                r.reject();
            });
        return r;
    }
}

function prepareSharing() {
    var shareBtnTml = $('#share-btn-tml').html();
    var renderedBtn = _.template(shareBtnTml)({
        title: $("meta[property='og\\:title']").attr("content"),
        url: $("meta[property='og\\:site_name']").attr("content"),
        img: $("meta[property='og\\:image']").attr("content"),
        text: $("meta[property='og\\:description']").attr("content")
    });
    return renderedBtn;
}
