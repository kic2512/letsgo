/**
 * Created by max on 17.01.16.
 */

(function () {

    function getCitiesStatistic(url) {
        $.get(url)
            .success(function (response) {
                if ('data' in response && 'question_txt' in response) {

                    window.cityData = response['data'];
                    if (map.isCityView === true) {
                        drawCircles(window.cityData, window.queTitle);
                    }
                }
                else {
                    console.log(response);
                }
            });
    }

    function queryStatistic(queId, choiceText) {
        var r = $.Deferred();

        var url = '/question-distribution/' + queId + '/';
        $.get(url)
            .success(function (response) {
                if ('data' in response && 'question_txt' in response) {

                    window.queTitle = response['question_txt'];
                    window.shareText = response['sharing_text'];
                    window.countryData = response['data'];
                    window.worldStat = response['world_statistic'];

                    if (map.isCountryView === true) {
                        drawCircles(window.countryData, window.queTitle);
                    }
                    getCitiesStatistic(url + '?cities=on');
                    r.resolve();
                }
                else {
                    console.log(response);
                    r.reject();
                }
            });
        return r;
    }
    window.queryStatistic = queryStatistic;
})();