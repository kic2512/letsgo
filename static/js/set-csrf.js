define([
    'jquery',
    'jquery.cookie'
], function ($) {
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $(document).ready(function() {
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    var csrftoken = $.cookie('csrftoken');
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $(document).ajaxComplete(function (event, xhr, settings) {
            if ('responseJSON' in xhr) {
                var data = xhr['responseJSON'];
                if ('csrf_token' in data) {
                    var csrfToken = data['csrf_token'];

                    $('input[name="csrfmiddlewaretoken"]').val(csrfToken);
                    $.cookie('csrftoken', csrfToken);
                }
            }
        });
    });
});