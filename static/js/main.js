require.config({
    //urlArgs: "bust=" + (new Date()).getTime(),
    baseUrl: "/static/js",
    paths: {
        'set-csrf': "set-csrf",
        'jquery': "libs/jquery-1.11.3.min",
        'jquery.cookie': "libs/jquery.cookie",
        'moment': "libs/moment",
        'underscore': "libs/underscore-min",
        'backbone': "libs/backbone",
        'bootstrap': "libs/bootstrap.min",
        'toastr': "toastr"
    },
    shim: {
        'bootstrap': {deps:['jquery']}
    }
});

define([
    'set-csrf',
    'backbone',
    'router',
    'jquery'
], function(
    csrf,
    Backbone,
    router,
    $
){

    function setClientNavigation () {
        var selectorToURI = {
            '.js-to-map': 'map'
        };

        for (var selector in selectorToURI) {
            if (selectorToURI.hasOwnProperty(selector)) {

                $(selector).click(function (event) {
                    event.preventDefault();
                    router.navigate(this.uri, {trigger: true});
                }.bind({
                    uri: selectorToURI[selector]
                }));
            }
        }
    }

    window.setClientNavigation = setClientNavigation;

    $(document).ready(function () {
        setClientNavigation();
    });

    Backbone.history.start();

});