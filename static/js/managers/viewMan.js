define([
    'backbone'
], function (Backbone) {

    var Manager = {
        subscribe: function (views) {
            for (var I in views) {
                this.listenTo(views[I], 'rerender', this.rerender);
                this.listenTo(views[I], 'reshow', this.reshow);
                this.listenTo(views[I], 'clickPhoto', this.showMeetingCard);
            }
        },

        unsubscribe: function (view) {
            this.stopListening(view);
        },

        rerender: function (view) {
            view.render();
            this.reshow(view);
        },

        triggerView: function (eventName) {
            this.trigger(eventName, this.currentView);
        },

        hideCurrent: function () {
            if (this.currentView) {
                this.triggerView('view-hide');

                var r = $.Deferred();
                this.currentView.$el.hide(function () {
                    r.resolve();
                });
                return r;
            }
            return $.when();
        },

        reshow: function (view) {
            this.hideCurrent().done(function () {
                this.currentView = view;
                this.triggerView('view-show');
                view.$el.show();
            }.bind(this));
        },

        show: function (view) {
            if (this.currentView) {
                this.currentView.hide();
            }
            view.show();
            this.currentView = view;
        },

        showMeetingCard: function (obj, e) {
            Backbone.route('')
        }
    };

    return _.extend(Manager, Backbone.Events);
});