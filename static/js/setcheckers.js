/**
 * Created by ikravtsov on 10/06/2018.
 */

function setUnreadConfirmsChecker() {
    if (window.userToken) {
        var timerId = null;
        timerId = setInterval(function () {
            xhr = new XMLHttpRequest();
            xhr.onloadend = function (response) {
                var data = JSON.parse(response.currentTarget.responseText);
                if (data && data.data) {
                    var unredCount = data.data.unread;
                    var unredReqSpan = document.getElementById('unred-requests');
                    if (unredCount > 0) {
                        if (unredCount < 100) {
                            unredReqSpan.innerHTML = unredCount;
                        }
                        else {
                            unredReqSpan.innerHTML = '99+';
                        }
                        unredReqSpan.innerHTML = unredCount;
                        unredReqSpan.style.display = "inline";
                    }
                    else {
                        unredReqSpan.style.display = "none";
                    }
                }
            };

            xhr.onerror = function () {
                clearInterval(timerId);
            };

            xhr.open('GET', '/unread-confirms/', true);
            xhr.setRequestHeader('Authorization', window.getTokenHeader());
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send();
        }, 10000)
    }

}

function setUnreadMessagesChecker() {

    if (window.userToken) {
        var timerId = null;

        timerId = setInterval(function () {
            xhr = new XMLHttpRequest();
            xhr.onloadend = function (response) {
                var data = JSON.parse(response.currentTarget.responseText);
                if (data && data.data) {
                    var unredCount = data.data.unread_total_count;
                    var unredMessageSpan = document.getElementById('unred-messages');
                    if (unredCount > 0) {
                        if (unredCount < 100) {
                            unredMessageSpan.innerHTML = unredCount;
                        }
                        else {
                            unredMessageSpan.innerHTML = '99+';
                        }
                        unredMessageSpan.style.display = "inline";
                    }
                    else {
                        unredMessageSpan.style.display = "none";
                    }
                }
            };

            xhr.onerror = function () {
                clearInterval(timerId);
            };

            xhr.open('GET', '/total-unread/', true);
            xhr.setRequestHeader('Authorization', window.getTokenHeader());
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send();
        }, 10000)
    }
}

setUnreadConfirmsChecker();
setUnreadMessagesChecker();
