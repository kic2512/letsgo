define([
    'jquery',
    'exporting'
], function ($) {
    function fetchDiagram(callback, queId, params) {
        var req = prepareReq(params);
        $.getJSON('/statistics/gender-diagram/' + queId + req, function (resp) {
            callback(resp);
        });
    }

    function renderDiagram(queId) {
        fetchDiagram(showDiagram, queId, '');
    }

    function updateDiagram(queId, params) {
        fetchDiagram(function (resp) {
            var chart = $('#gender-diagram').highcharts();
            var series = getGenders(resp);
            series.forEach(function (item, i, arr) {
                chart.series[i].setData(arr[i].data, false);
            });
            chart.redraw();
        }, queId, params);

    }

    function showDiagram(resp) {
        $('#gender-diagram').highcharts({
            chart: {
                type: 'bar'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Распределения ответов по полу'
            },
            xAxis: {
                categories: getCategories(resp),
                title: {
                    text: 'Варианты ответов'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Количество проголосовавших'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: getGenders(resp)
        });
    }

    function getGenders(jsonResp) {
        var series = [];
        var index = 0;
        var colors = ['#8085e9', '#90ed7d',  '#f15c80'];
        for (var key in jsonResp["data"]["choices"]) {
            var ser = {};
            ser.name = [key];
            ser.data = jsonResp["data"]["choices"][key];
            ser.color = colors[index];
            series.push(ser);
            index = index + 1;
        }
        return series;
    }

    return {renderDiagram: renderDiagram, updateDiagram: updateDiagram}
});


