define([
    'jquery',
    'exporting'
], function ($) {

    function fetchDiagram(callback, queId, params) {
        var req = prepareReq(params);
        $.getJSON('/statistics/poll-date-diagram/' + queId + req, function (resp) {
            callback(resp);
        });
    }

    function renderDiagram(queId) {
        fetchDiagram(showDiagram, queId, '');
    }

    function updateDiagram(queId, params) {
        fetchDiagram(function (resp) {
            var chart = $('#date-diagram').highcharts();

            while(chart.series.length > 0) {
                chart.series[0].remove();
            }

            var series = getDates(resp);

            series.forEach(function (item, i, arr) {
                chart.addSeries(arr[i]);
            });

        }, queId, params);

    }

    function showDiagram(series) {
        $('#date-diagram').highcharts('StockChart', {
            title: {
                text: 'Распределение ответов по времени',
                x: -20 //center
            },
            credits: {
                enabled: false
            },
            rangeSelector: {
                selected: 4
            },

            yAxis: {
                labels: {
                    formatter: function () {
                        return (this.value > 0 ? ' + ' : '') + this.value + '%';
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }],
                title: {
                    text: 'Количество проголосовавших (%)'
                }
            },

            plotOptions: {
                series: {
                    compare: 'percent'
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2
            },

            series: getDates(series)
        });
    }

    function getDates(resp) {
        var categories = getCategories(resp);
        var dateCategories = [];
        categories.forEach(function (entry) {
            var ss = entry.split('.');
            dateCategories.push(Date.UTC(ss[2], ss[1], ss[0]));
        });

        var series = getSeries(resp);
        series.forEach(function (ser) {
            ser.data.forEach(function (count, index, theArray) {
                var item = [];
                item.push(dateCategories[index]);
                item.push(count);
                theArray[index] = item;
            });
        });
        return series;
    }

    return {renderDiagram: renderDiagram, updateDiagram: updateDiagram}
});