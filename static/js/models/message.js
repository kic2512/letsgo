/**
 * Created by ikravtsov on 02/06/2018.
 */
define([
    'backbone'
], function(
    Backbone
) {

    var Model = Backbone.Model.extend({
        defaults: {
            "author": {},
            "date_create": "",
            "is_my": false,
            "is_read": false,
            "is_received": false,
            "text": ""
        },

        getTokenHeader: function () {
            return window.getTokenHeader();
        },

        sync: function(method, model, options) {
            options = options || {};

            options.headers = {
                'Authorization': this.getTokenHeader()
            };

            if (method == 'create') {
                options.url = '';
            }
            return Backbone.sync(method, model, options);
        }
    });

    return Model;
});

