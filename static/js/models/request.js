/**
 * Created by ikravtsov on 02/06/2018.
 */
define([
    'backbone'
], function(
    Backbone
) {

    var Model = Backbone.Model.extend({
        defaults: {
            "date_create": "",
            "id": "",
            "is_approved": false,
            "is_rejected": false,
            "meeting": {},
            "user": {}
        },

        getTokenHeader: function () {
            return window.getTokenHeader();
        },

        sync: function(method, model, options) {
            options = options || {};

            options.headers = {
                'Authorization': this.getTokenHeader()
            };

            if (method == 'create') {
                options.url = '';
            }
            return Backbone.sync(method, model, options);
        }
    });

    return Model;
});
