/**
 * Created by ikravtsov on 31/05/2018.
 */
define([
    'backbone'
], function(
    Backbone
) {

    var Model = Backbone.Model.extend({
        baseUrl: window.ServerUrls['meeting-detail'],
        confirmUrl: window.ServerUrls['meeting-confirm'],

        defaults: {
            "category": "",
            "category_obj": "",
            "chats": "",
            "color_status": "",
            "confirms": "",
            "coordinates": {},
            "description": "",
            "href": "",
            "id": "",
            "is_active": "",
            "meeting_date": "",
            "title": "",
            "owner": {}
        },

        sync: function(method, model, options) {
            options = options || {};
            if (method === 'create' || method === 'update') {
                console.log('meeting model');
                options.headers = {
                    'Authorization': window.getTokenHeader()
                }
            }
            return Backbone.sync(method, model, options);
        },

        sendConfirmRequest: function () {
            var xhr = new XMLHttpRequest();

            xhr.onloadend = function (p1) {
                console.log('request was sent');
            };

            xhr.onerror = function (p1) {
                console.log('Uploaded error');
            };

            var url = this.confirmUrl + this.id + '/';



            var tokenKey = window.getTokenHeader();
            var res = false;

            if (tokenKey){

                xhr.open('POST', url, true);
                xhr.setRequestHeader('Authorization', tokenKey);
                xhr.send();
                res = this;
            }

            return res;

        }
    });

    return Model;
});
