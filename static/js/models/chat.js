/**
 * Created by ikravtsov on 02/06/2018.
 */
define([
    'backbone'
], function(
    Backbone
) {

    var Model = Backbone.Model.extend({
        defaults: {
            "channel_slug": "",
            "id": "",
            "is_mute": false,
            "last_message": {},
            "owner": {},
            "title": "",
            "unread_count": ""
        },

        getTokenHeader: function () {
            return window.getTokenHeader();
        },

        sync: function(method, model, options) {
            options = options || {};

            options.headers = {
                'Authorization': this.getTokenHeader()
            };

            if (method == 'create') {
                options.url = '';
            }
            return Backbone.sync(method, model, options);
        },

        openSocketConnection: function () {

            if (window.socket) {
                this.closeSocketConnection();
            }

            var wsURL = '';

            if (window.DEBUG === true) {
                wsURL = 'ws://' + window.location.host + '/chat/' + this.channel_slug + '/?token=' + window.userToken;
            }
            else {
                wsURL = 'wss://' + window.location.host + '/chat/' + this.channel_slug + '/?token=' + window.userToken;
            }

            window.socket = new WebSocket(wsURL);

            window.socket.onmessage = function(response){
                var message = JSON.parse(response.data);
                this.trigger('addMessage', message)
            }.bind(this);

            window.socket.onclose = function (e) {
                console.log('reopen socket');
                if (window.isReallyNeedToCloseSocket === false){
                    this.openSocketConnection();
                }
            }.bind(this);
        },

        closeSocketConnection: function(){
            window.isReallyNeedToCloseSocket = true;
            window.socket.close();
            window.isReallyNeedToCloseSocket = false;
        },

        sendMessage: function (message) {
            window.socket.send(message);
        }
    });

    return Model;
});
