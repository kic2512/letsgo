define([
    'jquery',
    'exporting'
], function ($) {
    function fetchDiagram(callback, queId, params) {
        var req = prepareReq(params);
        $.getJSON('/statistics/age-diagram/' + queId + req, function (resp) {
            callback(resp);
        });
    }

    function renderDiagram(queId) {
       fetchDiagram(showDiagram, queId, '');
    }

    function updateDiagram(queId, params) {
        fetchDiagram(function (resp) {
            var chart = $('#age-diagram').highcharts();
            var series = getSeries(resp);
            series.forEach(function (item, i, arr) {
                chart.series[i].setData(arr[i].data, false);
            });
            chart.redraw();
        }, queId, params);

    }

    function showDiagram(resp) {
        $('#age-diagram').highcharts({
            title: {
                text: 'Распределение ответов по возрасту',
                x: -20 //center
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: getCategories(resp),
                title: {
                    text: 'Возраст проголосовавших'
                }
            },
            yAxis: {
                title: {
                    text: 'Количество проголосовавших'
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: '#808080'
                }]
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: getSeries(resp)
        });
    }

    return {renderDiagram: renderDiagram, updateDiagram: updateDiagram}
});

