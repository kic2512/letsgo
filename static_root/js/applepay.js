/**
 * Created by ikravtsov on 13/06/2018.
 */

if (window.ApplePaySession) {
    $('#test').bind('touchend', function () {
        const paymentRequest = {
            total: {
                label: 'russian-friends.com',
                amount: 1
            },
            countryCode: 'US',
            currencyCode: 'USD',
            merchantCapabilities: ['supports3DS'],
            supportedNetworks: ['masterCard', 'visa']
        };
        const applePaySession = new window.ApplePaySession(1, paymentRequest);
        applePaySession.begin();

        applePaySession.onvalidatemerchant = (event) => {
           // отправляем запрос на валидацию сессии
           performValidation(event.validationURL)
               .done(
                   (merchantSession) => {
                       // завершаем валидацию платежной сессии
                   this.applePaySession.completeMerchantValidation(merchantSession);
                   }
               ).fail(
               () => {
                   this.applePaySession.abort();
                   // показ сообщения об ошибке и т.п.
               }
           );
        };
    });
}