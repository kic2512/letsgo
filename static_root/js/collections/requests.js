/**
 * Created by ikravtsov on 02/06/2018.
 */
define([
    'backbone',
    'models/request'
], function(
    Backbone,
    RequestModel
){

    var Collection = Backbone.Collection.extend({
        model: RequestModel,
        url: window.ServerUrls['confirms-list'],
        urlBaseAction: window.ServerUrls['confirms-actions'],

        extra: {
        },

        getTokenHeader: function () {
            return window.getTokenHeader();
        },

        sync: function(method, model, options) {
            options = options || {};

            options.headers = {
                'Authorization': this.getTokenHeader()
            };

            return Backbone.sync(method, model, options);
        },

        parse: function (response, options) {
            if (response) {
                var collectionData = response;
                options.parse = false;
                return Backbone.Collection.prototype.parse.call(this, collectionData, options);
            }
            else if ('error' in response) {
                this.trigger('fetch-error', response.error);
                return {};
            }
        }
    });

    return new Collection();
});
