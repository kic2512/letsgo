define([
    'backbone',
    'models/meeting'
], function(
    Backbone,
    Meeting
){

    var Collection = Backbone.Collection.extend({
        model: Meeting,
        url: window.ServerUrls['meetings-list'],

        extra: {
        },

        sync: function(method, model, options) {
            options = options || {};

            return Backbone.sync(method, model, options);
        },

        parse: function (response, options) {
            if (response) {
                var collectionData = response;
                options.parse = false;
                return Backbone.Collection.prototype.parse.call(this, collectionData, options);
            }
            else if ('error' in response) {
                this.trigger('fetch-error', response.error);
                return {};
            }
        }
    });

    return new Collection();
});