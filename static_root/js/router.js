define([
    'backbone',
    'views/userSettings',
    'views/map',
    'views/meeting',
    'views/requests',
    'views/chats',
    'views/chat',
    'views/proxyMap',
    'managers/viewMan',
    'toastr'
], function (Backbone, userSettings, map, meeting, requests, chats, chat, proxyMap, viewMan, toastr)
{

    viewMan.subscribe([userSettings, map, requests, chats, chat]);

    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "positionClass": "toast-bottom-full-width",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "3000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    var Router = Backbone.Router.extend({

        initialize: function () {

        },

        routes: {
            'map': 'map',
            'user-settings': 'userSettings',
            'requests': 'requests',
            'chats': 'chats',
            'chat/:id': 'chat',
            'meeting/:id': 'meeting',
            'create-meeting': 'createMeeting'
        },

        userSettings: function () {
            viewMan.show(userSettings);
        },

        map: function () {
            viewMan.show(map);
        },

        createMeeting: function () {
            viewMan.show(proxyMap);
        },

        requests: function () {
            viewMan.show(requests);
        },

        chats: function () {
            viewMan.show(chats);
        },

        chat: function (chatId) {
            chat.show(chatId);
        },

        meeting: function (meetingId) {
            meeting.show(meetingId);
        }

    });
    return new Router();
});