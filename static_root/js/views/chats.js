/**
 * Created by ikravtsov on 02/06/2018.
 */
define([
    'backbone',
    'underscore',
    'collections/chats',
    'toastr'
], function (Backbone, _, chatsCollection, toastr) {

    var View = Backbone.View.extend({
        el: $('#right-column'),
        template: _.template($('#chats-tml').html()),

        events: {
            'click .js-to-chat': 'toChat'
        },

        initialize: function () {
            return this;
        },

        render: function () {
            this.$el.html(_.template($('#preloader-tml').html()));
            this.collection.fetch({
                success: function (response) {
                    console.log('fetch chats');
                    this.$el.html(this.template({
                        chats: response.models
                    }));
                }.bind(this),

                error: function () {
                }.bind(this)
            });
            return this;
        },

        show: function () {

            var unredMessageSpan = document.getElementById('unred-messages');
            unredMessageSpan.innerHTML = 0;
            unredMessageSpan.style.display = 'none';

            this.render();
            this.$el.show();
            this.el.scrollIntoView();
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        toChat: function (e) {
            console.log(e);
            var chatId = e.currentTarget.dataset.chatid;
            Backbone.history.navigate('chat/' + chatId, true);
        },

        showSuccess: function (message) {
            toastr['success'](message);
        },

        showWarning: function (message) {
            toastr['error'](message);
        }

    });
    return new View({collection: chatsCollection});
});

