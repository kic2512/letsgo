/**
 * Created by ikravtsov on 02/06/2018.
 */
define([
    'backbone',
    'underscore',
    'models/chat',
    'collections/messages',
    'toastr'
], function (Backbone, _, ChatModel, messagesCollection, toastr) {

    var View = Backbone.View.extend({
        el: $('#right-column'),
        template: _.template($('#messages-tml').html()),
        chat_hash: undefined,
        messageContainer: $('#conversation'),
        messageTemplate: _.template($('#message-tml').html()),

        events: {
            'click .js-send-message': 'sendMessage',
            'touchstart .js-send-message': 'sendMessage',
            'keydown #input-message-text': 'keyAction'

        },

        initialize: function () {
            this.listenTo(this.model, 'addMessage', function (message) {
                var messageContainer = document.getElementById('conversation');
                var newMessage = document.createElement('div');
                newMessage.innerHTML = this.messageTemplate({
                    message: message
                });

                messageContainer.appendChild(newMessage);
                messageContainer.scrollTop = messageContainer.scrollHeight;

            }, true);
            return this;
        },

        render: function () {
            this.$el.html(_.template($('#preloader-tml').html()));
            this.collection.fetch({
                success: function (response) {
                    this.model.channel_slug = response.models[0].attributes.channel_slug;
                    this.$el.html(this.template({
                        chat: response.models[0]
                    }));
                    this.model.openSocketConnection();
                }.bind(this),

                error: function () {
                    console.log(console.trace());
                    toastr['error']('Something went wrong :( Please try later...');
                }.bind(this)
            });
            return this;
        },

        show: function (chatId) {
            if (parseInt(chatId)) {
                this.collection.url = this.collection.urlBase + chatId + '/';
                this.render();
                this.$el.show();
                this.el.scrollIntoView();
            }
            else {
                this.showWarning('Something went wrong :( Please, try again later...');
            }
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        sendMessage: function () {
            var message = document.getElementById("input-message-text").value;
            if (message){
                this.model.sendMessage(message);
            }
            document.getElementById("input-message-text").value = "";
        },

        keyAction: function(event) {
            if((event.metaKey || event.ctrlKey) && event.keyCode === 13) {
                this.sendMessage()
            }
        },

        showSuccess: function (message) {
            toastr['success'](message);
        },

        showWarning: function (message) {
            toastr['error'](message);
        }

    });

    _.extend(View, Backbone.Events);

    var chatModel = new ChatModel();

    return new View({
        collection: messagesCollection,
        model: chatModel
    });
});