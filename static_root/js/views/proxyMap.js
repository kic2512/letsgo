/**
 * Created by ikravtsov on 08/06/2018.
 */
define([
    'backbone',
    'underscore',
    'views/map'
], function (Backbone, _, map) {

    var View = Backbone.View.extend({
        show: function () {
            map.showCreateMeeting();
        },

        hide: function () {
            map.hide();
            return this;
        }
    });

    _.extend(View, Backbone.Events);

    return new View({
    });
});
