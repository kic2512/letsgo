/**
 * Created by ikravtsov on 01/06/2018.
 */
define([
    'backbone',
    'underscore',
    'models/meeting',
    'toastr'
], function (Backbone, _, MeetingModel, toastr) {

    var View = Backbone.View.extend({
        el: $('#right-column'),
        template: _.template($('#meeting-tml').html()),

        events: {
            'click .meeting': 'show',
            'click .js-send-request' : 'sendRequest'
        },

        initialize: function () {
            return this;
        },

        render: function () {
            this.$el.html(_.template($('#preloader-tml').html()));
            this.model.fetch({
                success: function (model) {
                    this.$el.html(this.template({
                        meeting: model.toJSON()
                    }));
                }.bind(this),

                error: function () {
                    console.log(console.trace());
                    toastr['error']('Something go wrong :( Please try later');
                }.bind(this)
            });
            return this;
        },

        show: function (meetingId) {
            this.model.url = this.model.baseUrl + meetingId + '/';
            this.render();
            this.$el.show();
            this.el.scrollIntoView();
            return this;
        },

        sendRequest: function () {
            var trySend = this.model.sendConfirmRequest();
            if (trySend){
                toastr['success']('Request for conversation was sent');
            }
            else {
                toastr['warning']('You need to register');
            }

        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        showSuccess: function (message) {
            toastr['success'](message);
        },

        showWarning: function (message) {
            console.log(console.trace());
            toastr['error'](message);
        }

    });
    var meetingModel = new MeetingModel();
    return new View({model: meetingModel});
});
