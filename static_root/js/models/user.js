define([
    'backbone'
], function(
    Backbone
) {

    var Model = Backbone.Model.extend({
        url: window.ServerUrls['profile'],
        uploadPhotoUrl: window.ServerUrls['uploadPhotoUrl'],

        defaults: {
            first_name: '',
            about: '',
            gender: '',
            birth_date: '',
            avatar: ''

        },

        sync: function(method, collection, options) {
            options = options || {};
            options.headers = {
                'Authorization': window.getTokenHeader()
            };
            return Backbone.sync(method, collection, options);
        }

    });

    return Model;
});