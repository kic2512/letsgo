/**
 * Created by ikravtsov on 31/05/2018.
 */
define([
    'backbone'
], function(
    Backbone
) {

    var Model = Backbone.Model.extend({
        url: window.ServerUrls['meetings-list'],

        defaults: {

        },

        sync: function(method, model, options) {
            options = options || {};

            return Backbone.sync(method, model, options);
        }
    });

    return Model;
});
